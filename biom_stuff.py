#!/usr/bin/env python3


#TODO everything assumes root dir /dat1/metag! [SOLVED?]
#WARN: groups (treatment) can contain as few as 2 samples! TODO: save info on get_reasonably grouped
#TODO make a dict of file names and their variations

import pandas as pd
import argparse
import os
from os.path import splitext
import sys
import h5py
import subprocess as sp
from itertools import combinations
from treatment_extract import select_reasonably_grouped

#n_jobs = 8



def divide_and_run_in_parallel():
    '''
    using qiime's start_parallel_jobs.py
    start_parallel_jobs.py -ms testjobs.txt 0001
    would have to modify calls through sp.check_call
    Oooor, just return the list of jobs with proper args (separated with a newline)
    and RUNID
    '''
    #x = int(args['x'])
    #y = int(args['y'])
    #biom_files_to_merge = [hdf5_md_dir+"/"+p for p in os.listdir(hdf5_md_dir) if p.find(".biom")][x:y] #ATT
    #n_parts = int(round(biom_files_to_merge / n_jobs))
    pass

def biom_to_tsv():
    print('biom convert -i A_1506_to_biom.tsv -o A_1506.txt --to-tsv --table-type="OTU table" #? --process-obs-metadata taxonomy')
    print('biom convert -i A_1506.txt -o A_1506.biom --to-hdf5 --table-type="OTU table"')
    print('few checked manually for hdf5 and tsv v2.0 are consistent')

def tsv_to_biom():
    pass

def mapping_file_make(md, sample_col, group_colname='group'):
    # modify the columns so that biom is happy
    #SampleID   BarcodeSequence LinkerPrimerSequence    Study_id    Study_title Sample_classification   Sample_description  keywords    Treatment   Treatment_details   Description

    md.rename(columns={group_colname:"Treatment", sample_col:"#SampleID"}, inplace=True)
    #ATT: md
    #md.rename(columns={"treatment":"Treatment", "sample_id":"#SampleID"}, inplace=True)

    # rearrange get_loc
    sample_id_loc = md.columns.get_loc("#SampleID")
    treatment_id_loc = md.columns.get_loc("Treatment") #TODO throes key error if not found

    md = md[[sample_id_loc]+[treatment_id_loc] +\
        [i for i in range(md.shape[1]) if i != sample_id_loc and i != treatment_id_loc]]

    # insert dummy columns
    md.insert(1, "BarcodeSequence", "A")
    md.insert(2, "LinkerPrimerSequence", "Y")
    md.insert(md.shape[1], "Description", "description")

    md = md.drop_duplicates()

    # add the damned header
    o = "#Completely unnecessary header\n" + md.to_csv(sep='\t', quoting=2)

    return o


def add_columns_from(from_md, target_md, col_name="run_id", col_on = "sample_id"):
    return pd.merge(from_md[[col_on, col_name]], target_md, on=col_on)


def validate_mapping_file(mapping_fp, validated_mapping_file):
    sp.check_call("validate_mapping_file.py -m "+ mapping_fp
                    +" -b -p -o "+ validated_mapping_file +" -s",
                    shell=True)


def collapse_samples(hdf5_dir, hdf5_coll_dir, validated_mapping_file, collapsed_mapping_file, project_list):

    errors_on = []
    for project_id in project_list:
        ifile_path = hdf5_dir+"/"+project_id+"_OTU-HDF5.biom"
        ofile_path = hdf5_coll_dir+"/"+project_id+"_OTU-HDF5_collapsed_by_sample.biom"
        call = "collapse_samples.py \
            -b "+ifile_path+" \
            -m "+validated_mapping_file+" \
            --output_biom_fp "+ofile_path+" \
            --output_mapping_fp "+collapsed_mapping_file+" \
            --collapse_fields sample_id \
            --collapse_mode sum"
            #--normalize \ # DESeq2 diagnostic plots need interger counts

        print("\n")
        print(call)
        try:
            sp.check_call(call, shell = True)
        except:
            errors_on.append(project_id)

    return errors_on

#collapse_samples.py -b data/SRP072467_OTU-HDF5.biom -m data/treatment_extract/treatment_extracted_mapping_v2/tmp_biom_stuff.tsv_corrected.txt --output_biom_fp data/SRP072467_OTU-HDF5_collapsed.biom --output_mapping_fp data/mapping_file_collapsed.tsv --collapse_fields sample_id --collapse_mode sum



def add_mapping_file(hdf5_dir, hdf5_md_dir, metadata_path, project_list):
    if not os.path.isdir(hdf5_md_dir):
        os.mkdir(hdf5_md_dir)

    errors_on = []
    for project_id in project_list:
        ifile_path = hdf5_dir+"/"+project_id+"_OTU-HDF5_collapsed_by_sample.biom"
        ofile_path = hdf5_md_dir+"/"+project_id+"_OTU-HDF5_metadata.biom"
        call = "biom add-metadata -i " + ifile_path + " -o " + ofile_path + " -m " + metadata_path
        print("\n")
        print(call)
        try:
            sp.check_call( call, shell = True)
        except:
            errors_on.append(project_id)

    return errors_on


def normalize_biom(idir, odir):
    for ifile in os.listdir(idir):
        ofile = odir + "/" + ifile + "_norm.biom"
        ifile = idir + "/" + ifile

        call = "normalize_table.py \
            -i " + ifile + "/ \
            -a DESeq2 \
            -o " + ofile + "/"

        sp.check_call(call, shell=True)

    #call = "normalize_table.py -i " + idir + "/ -a DESeq2 -o "+ odir+"/"
    #sp.check_call(call, shell=True)


def get_group_pairs(hdf5_file):
    f = h5py.File(hdf5_file, "r")
    treatment = [t for t in f['sample']['metadata']['Treatment']]
    combs = [i for i in combinations(set(treatment), 2)]
    return combs


def differential_abundance_(hdf5_file, diff_otus_file, mapping_file, diagnostic_plots=False):
    print(hdf5_file)
    print(diff_otus_file)
    print(mapping_file)

    pairs = get_group_pairs(hdf5_file)
    for p in pairs:
        treatment0 = p[0]
        treatment1 = p[1]

        diff_otus_file_treatment =\
            splitext(diff_otus_file)[0]+"_"+treatment0+"_"+treatment1+splitext(diff_otus_file)[1]

        call = "python2 differential_abundance_my.py \
            -b "+str(hdf5_file)+" \
            -d "+str(diff_otus_file_treatment)+" \
            -m "+str(mapping_file)+" \
            -c Treatment -x "+str(treatment0)+" -y "+ str(treatment1) +" \
            -p " + str(int(diagnostic_plots))

        #call = "differential_abundance.py \
        #    -i "+str(hdf5_file)+" \
        #    -o "+str(diff_otus_file_treatment)+" \
        #    -m "+str(mapping_file)+" \
        #    -a metagenomeSeq_fitZIG \
        #    -c Treatment -x "+str(treatment0)+" -y "+ str(treatment1)
        #if diagnostic_plots:
        #    call = call + " --DESeq2_diagnostic_plots"

        print("\n")
        print(call)
        sp.check_call(call, shell=True)

        # keep in mind that this doesn't allow to channge the algorithm easily
        #DA_DESeq2("OTU-HDF5/merged_all_HDF5_v2.0_metadata/ERP009305_OTU-HDF5_metadata.biom",
        #    "DEFAULT_OFILE.tsv", "mapping_file_collapsed.tsv", "Treatment", "0", "1", True)

        #differential_abundance.py
        #    -i data/OTU-HDF5/merged_all_HDF5_v2.0_metadata/DRP002839_OTU-HDF5_metadata.biom
        #    -o data/differential_abundance/OTU-DIFF/data/OTU-HDF5/merged_all_HDF5_v2.0_metadata/DRP002839_OTU-DIFF.tsv
        #    -m data/treatment_extract/treatment_extracted_mapping_v2/tmp_biom_stuff.tsv_corrected.txt
        #    -c "Treatment" -x 0 -y 1


def differential_abundance(input_hdf5_dir, diff_otus_dir, mapping_file, use_projects = False, diagnostic_plots=True):
    key_errors_on = []
    key_error_example = "---- No errors."
    hdf5_file_list = [f for f in os.listdir(input_hdf5_dir) if f.find(".biom")>-1]

    if use_projects:
        hdf5_file_list = [f for f in hdf5_file_list if f.split("_")[0] in use_projects]

    print("---- Differential abundance uses files: ", hdf5_file_list)

    for hdf5_file in hdf5_file_list:
        diff_otus_file = diff_otus_dir+"/"+hdf5_file.split("_")[0]+"_OTU-DIFF.tsv"
        hdf5_file = input_hdf5_dir+"/"+hdf5_file

        try:
            differential_abundance_(hdf5_file, diff_otus_file, mapping_file, diagnostic_plots)
        except KeyError as e:
            key_errors_on.append(hdf5_file)
            key_error_example = e

    return key_errors_on, key_error_example



def merge_biom_files(file_list, output_biom_filepath, ofile_passed=False):
    # not tested
    inp = ",".join(file_list)
    odir = splitext(output_biom_filepath)[0]

    call = "parallel_merge_otu_tables.py \
        -i " + inp + " \
        -o " + odir

    if not ofile_passed:
        # then move the merged matrix into output filepath (is DIR here)
        call2 = "mv " + odir + "/merged.biom " + output_biom_filepath
    else:
        # if ofile passed then move the merged matrix into ofile
        call2 = "mv " + odir + "/merged.biom " + output_biom_filepath

    print(call)
    print(call2)
    try:
        sp.check_call(call, shell = True)
    except AttributeError as e:
        print(e)
    except sp.CalledProcessError as e:
        print(e)
    #sp.check_call(call2, shell = True) #ATT
    return 0

def generate_file_list(folder, project_list, file_extension = '.biom'):

    print(folder)
    print(project_list)
    file_list = [f for f in os.listdir(folder)
                if os.path.splitext(f)[-1] == file_extension]
    file_list = [folder+"/"+f for f in file_list if f.split("_")[0] in project_list]
    return file_list

def merge_biom_files_by(md, idir, odir, by_what, of_what = 'run_id',
                        file_extension = ".biom"):
    '''
    E.g. by project_id or biome
    '''
    if not os.path.exists(odir):
        os.mkdir(odir)

    # get list of by_what
    mapp = md[[of_what, by_what]].drop_duplicates()
    error_on = []
    # for entry in list
    for entry in mapp[by_what]:
        sublist = mapp[of_what].ix[mapp[by_what] == entry].tolist()
        # universal but soo slow
        #sublist = [f for f in os.listdir(odir) for i in sublist if any(i in f)]

        # huh, just a little less slow
        file_list = [f for f in os.listdir(idir) if os.path.splitext(f)[-1] == file_extension]
        sublist = [idir+"/"+f for f in file_list if f.split("_")[0] in sublist]
        sublist = generate_file_list(idir, sublist)
        print("--- Merging ", len(sublist), " files...")
        print("\n")

        # merge into a hdf named with entry
        if by_what == "project_id":
            ofile_name = odir + "/" + str(entry).replace(" ", "_") + file_extension
        else:
            ofile_name = odir + "/merged_" + str(entry).replace(" ", "_") + file_extension

        try:
            merge_biom_files(sublist, ofile_name, ofile_passed=True)
        except sp.CalledProcessError as e:
            raise e
            error_on = error_on+entry

        print("--- Errors on: ", error_on)


def merge_diff_otus_files(diff_otus_dir, odir, column_to_use = "log2FoldChange", index_column = 'taxonomy'):
    '''
    make a matrix from the diff abundance (qiime) output table
    use lo2FoldChange column for now - it should make sense
    '''
    fold_change = {}
    for ifile in [f for f in os.listdir(diff_otus_dir) if os.path.splitext(f)[-1] == ".tsv"]:
        #TODO replece with something faster
        table = pd.read_csv(diff_otus_dir+"/"+ifile, sep="\t", index_col = index_column)
        fold_change[ifile] = table[column_to_use] # index is OTU ID

    # NaNs for OTUs that do not appear in particular comparison
    pd.DataFrame.from_dict(fold_change)\
        .to_csv(odir+"/shifts_by_"+column_to_use+".tsv", sep="\t")



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="\
                        metadata M ( -> mapping file -> ) mapping file validated V [+ hdf5 dir H] -> hdf5 with metadata dir I -> differential abundance dir O\
                        ")
    parser.add_argument("-m", "--metadata_path",
                        #default = "/D/ebi/DEFAULT_METADATA.tsv",
                        help="",
                        required=False)
    parser.add_argument("-v", "--mapping_dir",
                        default = "",
                        help="",
                        required=True)
    parser.add_argument("-b", "--biom_hdf5_files_dir",
                        default = "",
                        help="Folder with all (pooled=extracted) .hdf5 files for each run in database",
                        required=False)
    parser.add_argument("-d", "--differential_abundance_output_dir",
                        default = "",
                        help="differential abundance output directory",
                        required=False)
    parser.add_argument("-o", "--merged_differential_abundance_output_dir",
                        default = "",
                        help="output directory for a matrix with merged abundance output",
                        required=False)
    args = vars(parser.parse_args())

    metadata_path          = args['metadata_path']
    mapping_dir            = args['mapping_dir']
    hdf5_raw_dir           = args['biom_hdf5_files_dir']
    diff_otus_dir          = args['differential_abundance_output_dir']
    diff_otus_merged_dir   = args['merged_differential_abundance_output_dir']

    #additional_md_file = "/D/ebi/DEFAULT_METADATA.tsv" # is just md
    #hdf5_coll_dir      = hdf5_raw_dir+"_collapsed_normalized"
    hdf5_dir_stem      = hdf5_raw_dir.rstrip("_extracted") #ATT
    hdf5_by_prj_dir    = hdf5_dir_stem+"_by_prj"
    hdf5_coll_dir      = hdf5_dir_stem+"_collapsed"
    hdf5_md_dir        = hdf5_dir_stem+"_metadata"
    #hdf5_norm_dir      = hdf5_raw_dir+"_normalized" # norm not used independently

    # _extracted (raw) - by_project - collapsed (by prj&sam)

    raw_mapping_file       = mapping_dir+"/mapping_file.tsv"
    validated_mapping_file = raw_mapping_file+"_corrected.txt" # qiime output
    collapsed_mapping_file = raw_mapping_file.replace(".tsv", "_collapsed.txt")

    for location in [mapping_dir, diff_otus_dir, hdf5_coll_dir, hdf5_md_dir]:
        if not location == "":
            if not os.path.isdir(location):
                os.mkdir(location)

    ###########################################################################
    md = pd.read_csv(metadata_path, sep="\t", index_col=0, engine='python') #, low_memory=False)
    use_grouped_projects = select_reasonably_grouped(md, "group_word_count").index.drop_duplicates().tolist()


    ###########################################################################
    ### DIRTY
    merge_biom_files(["/P/metag/data/OTU-HDF5/merged_all_HDF5_v2.0_collapsed/" + f
        for f in os.listdir("/P/metag/data/OTU-HDF5/merged_all_HDF5_v2.0_collapsed/")],
        "/P/metag/data/OTU-HDF5/collapsed_reasonably_grouped")
#    x = ['merged']
#    add_mapping_file("data/OTU-HDF5/collapsed_reasonably_grouped", "data/OTU-HDF5/collapsed_reasonably_grouped/tmp",
#        "data/DEFAULT_MAPPING/mapping_file_collapsed.txt", x)
#
#    ###########################################################################
#    ### MAKE MAPPING FILE (md -> raw_mapping)
#    print("\n-- Making the mapping file out of provided metadata...")
#
#    # select grouped
#
#    # all with any groups:
#    #use_grouped_projects = md.project_id.iloc[md.group.apply(lambda x: isinstance(x, str)).tolist()].tolist() #untested
#    #md = md.iloc[md.group.apply(lambda x: isinstance(x, str)).tolist(), :]
#
#    assert md.shape[0] > 0
#    #print(md.head()); import sys; sys.exit()
#
#    # add run id col
#    print("\n-- Setting run ID column to be used as #SampleID...") # biom files arent collapsed here
#    #additional_md = pd.read_csv(additional_md_file, sep='\t')
#    #md = add_columns_from(additional_md, md, "run_id", "sample_id")
#    #print(md.columns)
#
#    md = mapping_file_make(md, sample_col="run_id") #ATT
#    with open(raw_mapping_file, "w") as f:
#        f.write(md)

#    #md.to_csv(raw_mapping_file, sep="\t", index = False)
#
#    ### for merging with an available md
#    #pd.merge(m, t.ix[:,0:t.shape[1]-1], on=["#SampleID", "BarcodeSequence", "LinkerPrimerSequence", "Treatment"]).to_csv("data/tmp_mapping.tsv", sep="\t", index=False)
#
#
#    ###########################################################################
#    ### VALIDATE (raw -> validated)
#    print("\n-- Validating the mapping file...")
#    validate_mapping_file(raw_mapping_file, mapping_dir)
#

#    ###########################################################################
#    ### MERGE BIOM FILES BY PROJECT (Downloaded run files to a pooled project .biom file)
#    print("\n-- Merging projects in hdf5 files in hdf5_raw_dir...")
#    merge_biom_files_by(md, hdf5_raw_dir, hdf5_by_prj_dir, by_what='project_id', of_what='run_id', file_extension='.biom')
#    # use _extracted as the idir, all files must be in one dir

    ###########################################################################
    ### PROJECT LIST
    #project_list = md.project_id.drop_duplicates().tolist()
    #project_list = "SRP002480 ERP000108 SRP008047 ERP010458 ERP006678 ERP005185\
    #    ERP001038 ERP010229 ERP003902 SRP011011".split()
    #project_list = ["ERP010458"]
    #project_list = ["SRP055567", "SRP072467"]
    #project_list = [p.split("_")[0] for p in\
    #    os.listdir(hdf5_raw_dir) if p.find(".biom")]

    #project_list = [p for p in project_list if p in use_grouped_projects] # same as in the collapsed mapping file
   # those with treatment data
    project_list = use_grouped_projects


    ###########################################################################
    ### COLLAPSE BY SAMPLES (mapp validated -> mapp collapsed) (hdf5 -> hdf5_collapsed)
#    print("\n-- Collapsing runs to samples in hdf5 files in hdf5_raw_dir...")
#    collapse_samples(hdf5_by_prj_dir, hdf5_coll_dir, validated_mapping_file, collapsed_mapping_file, project_list)


#    ###########################################################################
#    ### ADD MAPPING FILE
#    print("\n-- Adding the mapping file to hdf5 files in hdf5_raw_dir...")
#    errors_on = add_mapping_file(hdf5_coll_dir, hdf5_md_dir, collapsed_mapping_file, project_list)
#    print("\n---- While adding metadata, errors were encountered on: ", errors_on)
#
#
#    ###########################################################################
#    ### NORMALIZE (now performed with diff abundance for integer counts are needed)
#    #print("\n-- Normalizing biom tables...")
#    #normalize_biom(hdf5_md_dir, hdf5_norm_dir)
#    #sys.exit()

#    ###########################################################################
#    ### NORMALIZATION FOR BIOM FILES
    # get mx out of .biom, process, put back in




#
#    ###########################################################################
#    ### DIFF ABUNDANCE
#    print("\n-- Calculating differentially abundant OTUs for each project (file)...")
#    key_errors_on, key_error_example = \
#        differential_abundance(hdf5_md_dir, diff_otus_dir, collapsed_mapping_file, project_list, diagnostic_plots=True)
#    print("\n---- Error on ", key_errors_on,
#        ". Probably hdf5 file does not have metadata added. Perhaps it uses samples as #SampleID instead od run ids?")
#    print(key_error_example)
#
#    ###########################################################################
#    ### MERGE BIOM FILES
#    print("\n-- Merging biom files... WARNING: hardcoded paths")
#    biom_files_to_merge = [hdf5_md_dir+"/"+p for p in os.listdir(hdf5_md_dir) if p.find(".biom")]
#    #merged_biom_file = "data/OTU-HDF5/HDF5-merged_projects/merged0.biom"
#    merged_biom_file = "data/DEFAULT_ODIR/merged4.biom"
#    merge_biom_files(biom_files_to_merge, merged_biom_file)



