#!/usr/bin/env python3

#TODO merge the OTU diff files (biom_stuff.py)
#TODO concept of "focus" sample(s), "query", "of interest"
#TODO import dendrogram plotting from previous work

import pandas as pd
import numpy as np
import argparse
import os
from os.path import splitext
import sys
import matplotlib.pyplot as plt
from matplotlib.colors import rgb2hex, colorConverter
from fastcluster import linkage
import seaborn as sns
import sklearn.decomposition.pca
import networkx as nx
from scipy.cluster.hierarchy import dendrogram, set_link_color_palette
from scipy.spatial import distance

from biom_stuff import merge_biom_files
from skrzynka import *
from pairwise import current_raw_to_dist

my_palette = {
    'red'   : sns.color_palette('muted')[2],
    'black' : sns.light_palette('black')[-2],
    'blue'  : sns.color_palette('muted')[0],
    }


#def differential_abundance_plot():
    # make table with cols: project_id, group1, group2, OTU, taxonomy, adjPvalues

    # for prj in project_list:
    #

def notes_plots():
    notes = {
        'clustermap': '''
            #sns.heatmap(MI_tax_bin.ix[:200, :200]);
            p20 = sns.clustermap(MI_tax_bin.ix[:200, :200])
            plt.setp(p20.ax_heatmap.yaxis.get_majorticklabels(), rotation=0)
            plt.title("Tax MI")
            plt.show()
            ''',
        'grid': '''
            lm = sns.lmplot('X','Y',df,col='Z',sharex=False,sharey=False)
            axes = lm.axes
            axes[0,0].set_ylim(0,)
            axes[0,1].set_ylim(0,)
            ''',
        'cumplot': '''sns.distplot(exvar,
                         hist_kws=dict(cumulative=True),
                         kde_kws=dict(cumulative=True));
                         ''',
        'other': '''
                plt.axis('equal')
                ''',
    }
    for title, i in notes.items():
        print(title, i)

def rep_colours(length, pal="deep", n_colours=3):
    #colours = [colour_list[0] for i in range(length)]
    #for i,c in enumerate(colours):
        #if i%2 == 0:
                #colours[i] = "r"[i for i in range(len(colours)) if i%2 == 0]

    print("Use palette=sns.color_palette('deep', 3)")
    colour_list = sns.color_palette(pal, 3)
    colours =[]
    for i in range(round(length/n_colours+1)):
        colours += colour_list
    return colours[:length]


def subset_n_closest(M, focus_samples, n):
    '''
    get a list of n samples that are closest distance-wise to the focus samples
    '''
    # assume distance-like
    #TODO
    pass

def distance_matrix_from_biom(biom_file_or_dir, distance_dir, metric):
    '''
    read in a hdf5 file and return a distance matrix
    or just use the qiime distance scripts
    Can use UniFrac with phylogenetic tree
    '''
    #beta_diversity.py -i data/DEFAULT_ODIR/merged_47.biom -o data/tmp_beta_div_o -m euclidean
    pass



def tax_to_labels(tax_list):
    species = [t.split("; s__")[-1] for t in tax_list]
    genera = [t.split("; g__")[-1].split("; s__")[0] for t in tax_list]
    families = [t.split("; f__")[-1].split("; g__")[0] for t in tax_list]
    phyla = [t.split("; p__")[-1].split("; c__")[0] for t in tax_list]
    names = [" ".join([g,s]) for g,s in zip(genera, species)]
    names = ["(family: "+f+")" if n==" " else n for n,f in zip(names, families)]
    names = ["(phylum: "+p+")" if n=="(family: )" else n for n,p in zip(names, phyla)]
    return names

def cool_barplot(pandas_series, fontsize=20):
    fig = plt.figure()
    pos = [i for i in range(len(pandas_series))]
    bars = plt.barh(pos, pandas_series.values)
    for bar in bars:
        plt.gca().text(max(pandas_series), bar.get_y()+bar.get_height()/3, str(int(bar.get_width())),
        ha='right', color='k', fontsize=fontsize)
    plt.yticks(pos, pandas_series.index, rotation='horizontal', fontsize=fontsize);
    return fig



def plot_volcano(deseq2_table, contrasts = (None, None), alpha = 0.01,
                palette = my_palette, **kwargs):
    sns.set_style('whitegrid')
    fig = plt.figure()

    means = deseq2_table['baseMean']
    lfcs = deseq2_table['log2FoldChange']
    pvals = deseq2_table['padj']

    colors = [palette['red'] if i else palette['black'] for i in (deseq2_table.padj < alpha).tolist()]

    plt.scatter(lfcs, -np.log10(pvals), c = colors, alpha = 0.5, s=15, **kwargs)

    # remove all the ticks (both axes), and tick labels on the Y axis
    plt.tick_params(top='off', bottom='off', left='off', right='off',
                    labelleft='on', labelbottom='on')

    # remove the frame of the chart
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    ax = fig.add_subplot(111)
    if all([isinstance(i, str) for i in contrasts]) and len(contrasts) == 2:
        ax.set_title('Volcano plot for '+contrasts[0]+' and '+contrasts[1])
    else:
        ax.set_title('Volcano plot')

    ax.set_xlabel('log2 fold change')
    ax.set_ylabel('-log10(adjusted p-value)')

    fig.subplots_adjust(bottom=0.20)
    fig.text(.02, .02, 'Size of markers represents log2 fold change.\nRed markers represent points with adjusted p-value < '+str(alpha)+".")

    return fig

def plot_stderr(deseq2_table, contrasts = (None, None), alpha = 0.01,
                palette = my_palette, **kwargs):
    sns.set_style('whitegrid')
    fig = plt.figure()

    means = deseq2_table['baseMean']
    lfcs = deseq2_table['log2FoldChange']
    sers = deseq2_table['lfcSE']

    colors = [palette['red'] if i else palette['black'] for i in (deseq2_table.padj < alpha).tolist()]

    plt.scatter(lfcs, -sers, c = colors, s = lfcs**2, alpha = 0.5, **kwargs)

    # remove all the ticks (both axes), and tick labels on the Y axis
    plt.tick_params(top='off', bottom='off', left='off', right='off',
                    labelleft='on', labelbottom='on')

    # remove the frame of the chart
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    ax = fig.add_subplot(111)
    if all([isinstance(i, str) for i in contrasts]) and len(contrasts) == 2:
        ax.set_title('Standard error plot for '+contrasts[0]+' and '+contrasts[1])
    else:
        ax.set_title('Standard error plot')

    ax.set_xlabel('log2 fold change')
    ax.set_ylabel('negative standard error')

    fig.subplots_adjust(bottom=0.20)
    fig.text(.02, .02, 'Size of markers represents log2 fold change.\nRed markers represent points with adjusted p-value < '+str(alpha)+".")

    return fig


def plot_MA(deseq2_table, contrasts = (None,None), alpha = 0.01,
                palette = my_palette, **kwargs):
    sns.set_style('whitegrid')
    fig = plt.figure()

    means = deseq2_table['baseMean']
    lfcs = deseq2_table['log2FoldChange']
    sers = deseq2_table['lfcSE']


    colors = [palette['red'] if i else palette['black'] for i in (deseq2_table.padj < alpha).tolist()]

    plt.scatter(np.log2(means),
                lfcs, c = colors, alpha = 0.5,
                **kwargs,
                s = 15
                )

    # remove all the ticks (both axes), and tick labels on the Y axis
    plt.tick_params(top='off', bottom='off', left='off', right='off',
                    labelleft='on', labelbottom='on')

    # remove the frame of the chart
    for spine in plt.gca().spines.values():
        spine.set_visible(False)


    ax = fig.add_subplot(111)
    if all([isinstance(i, str) for i in contrasts]) and len(contrasts) == 2:
        ax.set_title('MA plot for '+contrasts[0]+' and '+contrasts[1])
    else:
        ax.set_title('MA plot')

    ax.set_xlabel('log2 base mean')
    ax.set_ylabel('log2 fold change')

    fig.subplots_adjust(bottom=0.20)
    fig.text(.02, .02, 'Red markers represent points with adjusted p-value < '+str(alpha)+".")

    return fig

def plot_log2fc_global(deseq2_table, contrasts=(None, None), alpha=0.01,
                palette = my_palette, **kwargs):
    sns.set_style('whitegrid')
    deseq2_table = deseq2_table.sort_values('log2FoldChange')

    fig = plt.figure(num=None, figsize=(8,8))
    pos = [i for i in range(deseq2_table.shape[0])]
    tick_labels = deseq2_table.index

    means = deseq2_table['baseMean']
    lfcs = deseq2_table['log2FoldChange']
    sers = deseq2_table['lfcSE']

    #colors = [blue if i else red for i in (lfcs < 0).tolist()]
    colors = [palette['red'] if i else palette['black'] for i in (deseq2_table.padj < alpha).tolist()]

    bars = plt.barh(pos, lfcs, height=1, #xerr = sers,
            align='center', linewidth=0, color=colors, **kwargs)

    # remove all the ticks (both axes), and tick labels on the Y axis
    plt.tick_params(top='off', bottom='off', left='off', right='off',
                    labelleft='off', labelbottom='on')

    # remove the frame of the chart
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    ax = fig.add_subplot(111)
    ax.set_xlabel('log2 fold change')

    if all([isinstance(i, str) for i in contrasts]) and len(contrasts) == 2:
        ax.set_title('Log2 fold change for all taxa between '+contrasts[1]+' and '+contrasts[0])
        caption0 = 'Comparison of '+str(contrasts[1])+' to '+str(contrasts[0])+' (reference).\n'
    else:
        ax.set_title('Log2 fold change for all taxa.')
        caption0=''

    caption = caption0

    fig.subplots_adjust(bottom=0.20)
    fig.text(.02, .02, caption)

    return fig


def plot_log2fc(deseq2_table, contrasts=(None, None), alpha=0.01, show_n=10,
                palette = my_palette, **kwargs):
    #TODO subset with padj
    sns.set_style('whitegrid')
    font = {'family' : 'normal',
            #'weight' : 'bold',
            'size'   : 24}
    plt.rc('font', **font)

    deseq2_table = deseq2_table[deseq2_table['padj'] < alpha]
    n_below_alpha = deseq2_table.shape[0]
    deseq2_table = pd.concat([
        deseq2_table.sort_values('log2FoldChange').head(round(show_n/2)),
        deseq2_table.sort_values('log2FoldChange').tail(round(show_n/2))
        ])

    #fig = plt.figure()
    fig = plt.figure(num=None, figsize=(10,2+show_n/2), dpi=72, facecolor='w', edgecolor='k')

    pos = [i for i in range(deseq2_table.shape[0])]
    tick_labels = tax_to_labels(deseq2_table.index.tolist())
    lfcs = deseq2_table['log2FoldChange']
    ses = deseq2_table['lfcSE']

    colors = [palette['blue'] if i else palette['red']for i in (lfcs < 0).tolist()]

    bars = plt.barh(pos, lfcs, height=0.4, xerr = ses,
                    align='center', linewidth=0, color=colors)
    plt.yticks(pos, tick_labels, rotation='horizontal');

    # Make the labels a little more visible
    ax = fig.add_subplot(111)
    #fig.subplots_adjust(left=0.45)
    fig.tight_layout()
    #fig.subplots_adjust(top=-0.1)

    if all([isinstance(i, str) for i in contrasts]) and len(contrasts) == 2:
        ax.set_title('Log2 fold change between '+contrasts[1]+' and '+contrasts[0])
        caption0 = 'Comparison of '+str(contrasts[1])+' to '+str(contrasts[0])+' (reference).\n'
    else:
        #ax.set_title('Log2 fold change')
        caption0=''

    caption = caption0+'Showing '+str(deseq2_table.shape[0])+\
            ' out of '+str(n_below_alpha)+' taxa for which adjusted p-value < '+\
            str(alpha)+'.\nStandard errors are shown.'

    ax.set_xlabel('log2 fold change')
    #ax.set_ylabel('taxa')
    ax.set_ylabel('')

    #fig.gca().set_position((.1, .3, .8, .6)) # to make a bit of room for extra text
    fig.subplots_adjust(bottom=0.20)
    fig.text(.02, .02, caption)

    # direct label each bar with Y axis values
    for bar in bars:
        if bar.get_width() < 0:
            shift = 2
        elif bar.get_width() > 0:
            shift = -2
        else:
            shift = 0
    ##    plt.gca().text(
    #        #bar.get_y() + bar.get_height()/2, bar.get_width() - 5, str(int(bar.get_width())),
    #        bar.get_x()+shift, bar.get_y(), str(int(bar.get_width())),
    #        ha='center', color='k', fontsize=20
    #        )

    # add means
    #means = deseq2_table['baseMean']
    #plt.barh([i+0.4 for i in pos], deseq2_table['baseMean'], height=0.5)

    # remove the frame of the chart
    for spine in plt.gca().spines.values():
        spine.set_visible(False)

    # remove all the ticks (both axes), and tick labels on the Y axis
    plt.tick_params(top='off', bottom='off', left='off', right='off',
                    labelleft='on', labelbottom='on')

    return fig


def plot_log2fc_multiple_heatmap(deseq2_tables, alpha = 0.01):
            #deseq2_table, contrasts=(None, None), alpha=0.01, show_n=10,
            #palette = my_palette, **kwargs):
    '''
    deseq2_tables : dict; just take care they have the same reference!
    '''

    sns.set_style('whitegrid')
    fig = plt.figure(num=None, figsize=(6, 18), dpi=72, facecolor='w', edgecolor='k')

    use_taxa = set()
    for table in deseq2_tables.values():
        use_taxa.update(set(table.index[table['padj'] < alpha]))

    hmp = {}
    for label, table in deseq2_tables.items():
        hmp[label] = table.ix[use_taxa]['log2FoldChange']

    hmp = pd.DataFrame(hmp).dropna() #if it's na in any comparison, it's dropped!
    hmp = hmp.sample(min(hmp.shape[0], 100))
    hmp.sort_values(hmp.columns.tolist(), inplace=True)

    fig = sns.heatmap(hmp, square=True)

    ytick_labels = tax_to_labels(hmp.index.tolist())
    ypos = [i for i in range(len(ytick_labels))]
    #xtick_labels = names_to_human_readable(hmp.columns.tolist())
    #xpos = [i for i in range(len(ytick_labels))]

    plt.yticks(ypos, ytick_labels, rotation='horizontal');
    #plt.xticks(xpos, xtick_labels, rotation='vertical');
    plt.xticks(rotation='vertical');

    return fig



def differential_abundance_plot():
    # get list of diff otus for project list
    # merge projects in list merge_biom_files
    # plot heatmap make_distance_comparison_plots.py
    pass


def make_heatmap():
    pass
    # copied from biom_stuff.py (redundant?)
    #Group the heatmap columns (samples) by metadata category (e.g., Treatment), then cluster within each group:
    #make_otu_heatmap.py -i otu_table.biom -o heatmap_grouped_by_Treatm mapping_file.txt -c Treatment
    #or
    #make_distance_comparison_plots.py


def distance_matrix_heatmap(D, use_index_level="project_id",
                    saveloc = '/P/mnemonic/plots/similar_samples.png',
                    **kwargs):
    '''
    qiime: make_distance_comparison_plots.py -d data/DEFAULT_ODIR/merged2.biom -m data/mapping_file_collapsed.tsv -o data/distance_plots/merged2 -f Treatment -c "List,Of,Groups"

    Input:  A distance matrix. Make sure D.index == D.columns
    Output: A plot; samples coloured by project and treatment on the margins.
            A brief description of the projects most close to the "focus sample".
    '''

    # Select a subset of the networks
    used_level2 = list(set([i for i in D.columns.get_level_values(use_index_level)])) # use all for now
    used_columns = (D.columns.get_level_values(use_index_level)\
                    .isin(used_level2))
    D = D.loc[used_columns, used_columns]
    assert sum(D.index != D.columns) == 0

    # Create a custom palette to identify the networks
    #level2_pal = sns.cubehelix_palette(len(used_level2),
    #                light=.9, dark=.1, reverse=True,
    #                start=1,
    #                #rot=-2
    #                )
    level2_pal = sns.color_palette(
                    "Set2",
                    #"colorblind",
                    #"muted",
                    n_colors=len(used_level2),
                    )
    level2_lut = dict(zip(map(str, used_level2), level2_pal))

    # Convert the palette to vectors that will be drawn on the side of the matrix
    level2 = D.columns.get_level_values(use_index_level)
    level2_colors = pd.Series(level2, index=D.columns).map(level2_lut)
    #level2_colors.index.get_level_values ?

    # Create a custom colormap for the heatmap values
    cmap = sns.diverging_palette(h_neg=210, h_pos=350, s=90, l=30, as_cmap=True)

    # Draw the full plot
    p = sns.clustermap(D,
                    row_colors=level2_colors,
                    col_colors=level2_colors,
                    #row_cluster = False,
                    linewidths=.5,
                    cmap=cmap,
                    vmin=0, vmax=1,
                    **kwargs
                    )

    plt.setp(p.ax_heatmap.yaxis.get_majorticklabels(), rotation=0)
    plt.savefig(saveloc, transparent=False,
                    dpi=300, bbox_inches='tight')
    return p

def heatmap_with_levels(D, use_index_level=1, axis=0, **kwargs):
    '''
    qiime: make_distance_comparison_plots.py -d data/DEFAULT_ODIR/merged2.biom -m data/mapping_file_collapsed.tsv -o data/distance_plots/merged2 -f Treatment -c "List,Of,Groups"

    Input:  A distance matrix. Make sure D.index == D.columns
    Output: A plot; samples coloured by project and treatment on the margins.
            A brief description of the projects most close to the "focus sample".
    # TODO: axis arg
    '''

    used_level2 = D.columns.get_level_values(use_index_level).drop_duplicates().tolist()
    level2_pal = sns.color_palette( "Set2", n_colors=len(used_level2))
    level2_lut = dict(zip(map(str, used_level2), level2_pal))

    # Convert the palette to vectors that will be drawn on the side of the matrix
    level2 = D.columns.get_level_values(use_index_level)
    level2_colors = pd.Series(level2, index=D.columns).map(level2_lut)

    # Create a custom colormap for the heatmap values
    cmap = sns.diverging_palette(h_neg=210, h_pos=350, s=90, l=30, as_cmap=True)

    # Draw the full plot
    p = sns.clustermap(D,
                    col_colors=level2_colors,
                    #row_cluster = False, # cluster by predefined
                    #linewidths=.1,
                    cmap=cmap,
                    #vmin=0, vmax=1,
                    **kwargs,
                    )

    plt.setp(p.ax_heatmap.yaxis.get_majorticklabels(), rotation=0)
    return p







def distance_heatmap_from_biom():
    '''
    Input: biom hdf5 file

    Takes in a biom file, calculates distances between the samples (TODO: groups?),
    returns a heatmap of clustered samples coloured by project ID.

    calls distance_matrix_plots
    '''
    pass

def histogram():
    #plt.figure()
    p = sns.distplot(A.sum(axis)) #, bins=70, norm_hist=False), sharex = False, sharey = False)
    #p.set(ylim = (0,None), xlim = (None, 500))
    #plt.savefig(ofp)
    return p

### PCA plots
def plot_pca(pca, A, md, mapp_index_col='sample_id', color_by='biome', subset_by='biome',
            plot_subset = ['Fecal', 'Human', 'Soil',
                            'Digestive System', 'Vagina',
                            'Skin', 'Large Intestine', "Plants",
                            "Agricultural", "Activated Sludge"],
            marker_size=3, palette="nipy_spectral_r", jitter=None, alpha=1):
    '''
    Input:  pca:            PCA result as from sklearn.decomposition.PCA
                            or a matrix with components as cols, of which [n x 2] will be used
            A:              the matrix from which PCA was computed
            plot_subset:    "all" or a list of entries in color_by column in md to plot OR (int) top most abundant

    Generally use the same for color_by and subset_by. Using different col names for those results
        in the inability to subset by the thing that you're coloring by.
    '''

    if type(pca) in (sklearn.decomposition.pca.PCA, sklearn.decomposition.NMF, sklearn.decomposition.KernelPCA):
        comps = pd.merge(
                    pd.DataFrame(pca.components_[:2,:].T,
                        index = A.index, columns = ['PC1', 'PC2']
                        ),
                    mapp(md, mapp_index_col, list(set([color_by, subset_by]))),
                    how="inner", left_index=True, right_index=True
            )
    else:
        comps = pd.merge(
                    pd.DataFrame(np.array(pca)[:,:2],
                        index = A.index, columns = ['PC1', 'PC2']
                        ),
                    mapp(md, mapp_index_col, list(set([color_by, subset_by]))),
                    how="inner", left_index=True, right_index=True
            )

    print(comps.shape)
    if isinstance(plot_subset, int):
        #print("The sorting will take some time, please wait")
        comps = comps.ix[
                    comps[subset_by].apply(
                        lambda x: x in
                            comps.groupby(color_by).count()\
                                .sort_values('PC1', ascending=False)[:plot_subset]\
                                .index.tolist()
                        ),
                    :]

    #if isinstance(plot_subset, list):
    elif len(plot_subset) ==0:
        comps = comps.ix[
            comps[subset_by].apply(
                    lambda x: x in plot_subset
                ),:
            ]
    print(comps.shape)

    fig = plt.figure(num=None, figsize=(40,40), dpi=72)
    p = sns.lmplot(x="PC1", y="PC2", hue=color_by, data=comps,
            ci=None, palette=palette, fit_reg=False, size=7, #col_wrap=2,
            scatter_kws={"s": marker_size, "alpha": alpha},
            x_jitter=jitter, y_jitter=jitter,
        )

    return p


def plot_quick_pca(pca, A, md, mapp_index_col='sample_id', marker_size=3):
    '''
    Input:  pca:            PCA result as from sklearn.decomposition.PCA
                            or a matrix with components as cols, of which [n x 2] will be used
            A:              the matrix from which PCA was computed
            plot_subset:    "all" or a list of entries in color_by column in md to plot OR (int) top most abundant
    '''
    if type(pca) in (sklearn.decomposition.pca.PCA, sklearn.decomposition.NMF, sklearn.decomposition.KernelPCA):
        comps = pd.DataFrame(pca.components_[:2,:].T,
                        index = A.index, columns = ['PC1', 'PC2']
                        )
    else:
        comps = pd.DataFrame(np.array(pca)[:,:2],
                        index = A.index, columns = ['PC1', 'PC2']
                        )

    p = sns.lmplot(x="PC1", y="PC2", data=comps,
            ci=None, palette="spectral", fit_reg=False, size=4, #col_wrap=2,
            scatter_kws={"s": marker_size, "alpha": 1}
        )

    return p

### graph equivalent to a venn diagram
def intersections_get_nodes(kw):
    nodes = {}
    edges = []

    # idea: combs = kw.drop_duplicates() # has all the existing combinations
    combs = pd.DataFrame(
                kw.groupby(kw.columns.tolist()).size().reset_index(),
                )\
                .rename(columns= ({0: 'count'}))

    counts = combs['count']
    combs.drop('count', axis=1, inplace=True)

    for i in range(combs.shape[0]):
        # take the columns which represent keywords that a group of samples has
        keywords_ = set(combs.columns[combs.ix[i, :] == 1])
        node_name = "+".join(keywords_)
        if node_name != "":
            node_value = counts[i]
            nodes[node_name] = node_value

            # also record the edges
            edges_ = [(node_name, k) for k in keywords_]
            edges += edges_

    edges = list(set(edges))

    return nodes, edges


def show_intersections(binarized_keywords, use_keywords="all", figsize=20, scale_factor=5, circular=True):
    '''
    Idea from http://stackoverflow.com/a/10814476/7097159

    binarized_keywords:     A boolean matrix with keywords as columns and samples as index
                            E.g. an output of binarize_keyword_metadata()
    use_keywords:           Either 'all', an int (subsetting top keywords),
                            or a list of keywords to plot
    '''
    kw = binarized_keywords

    if isinstance(use_keywords, int):
        kw = kw[kw.sum().sort_values(ascending=False)[:use_keywords].index]
    if isinstance(use_keywords, list):
        x = kw.ix[kw[use_keywords].sum(1) != 0, :].sum() != 0
        kw = kw[x[x].index.tolist()]

    print("Intersecting keywords: (", len(kw.columns.tolist()), ")", kw.columns.tolist())
    nodes, edges = intersections_get_nodes(kw)

    plt.figure(figsize=(figsize, figsize))
    G = nx.Graph()

    node_sizes = [s for s in nodes.values()]
    G.add_edges_from( edges )

    if circular:
        pos_ = nx.spring_layout(G, k=999)
    else:
        pos_ = nx.spring_layout(G)
        #pos=nx.spring_layout(G, k=1/sqrt(len(nodes))), # default

    nx.draw_networkx(G,
                    pos = pos_,
                    node_size = [i/scale_factor for i in node_sizes]
                    #node_size = 0
                    )
    plt.axis('off')
    plt.show()


