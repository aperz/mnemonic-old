#!/usr/bin/env python3

description = '''
COPIED TO metadata_taxonomy.py
'''
description

import argparse
import pandas as pd

def taxonomy_metadata_save(ifile_path, ofile_path):
    taxonomy = pd.read_csv(ifile_path, sep="\t")['taxonomy']\
                .drop_duplicates().tolist()

    open(ofile_path, 'w').close()

    with open(ofile_path, "a") as ofile:
        ofile.write(
            "taxonomy\tkingdom\tphylum\tclass\torder\tfamily\tgenus\tspecies\n"
            )

        for t in taxonomy:
            try:
                tax = [i[3:] for i in t.split("; ")]
                ofile.write(t+"\t"+"\t".join(tax)+"\n")

            except AttributeError:
                # some nans ><
                print(t)

    print("Saved to", ofile_path)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("-i", "--input_path",
                        help="Path to the table with al the runs merged (or just any table that has a 'taxonomy' column",
                        required=True)
    parser.add_argument("-o", "--output_path",
                        help="Path to the output .tsv file with taxonomy split into separate columns",
                        required=True)
    args = vars(parser.parse_args())

    print(description)






