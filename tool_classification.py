#!/usr/bin/env python3
'''
Utilities for classifier banchmarkng.
Important: cv_multiclass
'''

import sys
import numpy as np
import pandas as pd
from time import time
import matplotlib.pyplot as plt

from scipy.sparse import csr_matrix
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_selection import SelectKBest, chi2
from sklearn.linear_model import RidgeClassifier
from sklearn.naive_bayes import BernoulliNB, MultinomialNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.utils.extmath import density
from sklearn import metrics
from sklearn.metrics import roc_curve, auc
from sklearn.model_selection import train_test_split, StratifiedKFold

from metadata_samples import binarize_keyword_metadata


def load_data(min_n_Y=20):
    '''
    Load the metagenomics data, return X, Y for multiclass classification.
    '''
    X = pd.read_csv('/D/ebi/A_OTU-TSV_v2_intestine.tsv', sep="\t", index_col=0)
    md = pd.read_csv('/D/ebi/DEFAULT_METADATA.tsv', sep="\t",
                index_col=False, low_memory=False,
                usecols=['project_id', 'biome', 'sample_id', 'sample_name',
                        'keywords'], # 'group_word_count'],
            ).drop_duplicates()

    md.index = md.sample_id
    X, md = X.align(md, axis = 0, join='inner')
    tf_transformer = TfidfTransformer(sublinear_tf = True)
    X = tf_transformer.fit_transform(X)

    Y = binarize_keyword_metadata(md)
    Y = Y.ix[:, Y.sum() > min_n_Y]
    return X, Y



def benchmark(clf, X_train, X_test, y_train, y_test):
    '''
    Benchmark classifiers.
    '''

    print('_' * 80)
    print("Training: ")
    print(clf)
    t0 = time()
    clf.fit(X_train, y_train)
    train_time = time() - t0
    print("train time: %0.3fs" % train_time)

    t0 = time()
    pred = clf.predict(X_test)
    test_time = time() - t0
    print("test time:  %0.3fs" % test_time)

    acc_score = metrics.accuracy_score(y_test, pred)
    pre_score = metrics.precision_score(y_test, pred) #ATT average = weighted
    auc_score = metrics.roc_auc_score(y_test, pred)

    print("accuracy:   %0.3f" % acc_score)
    print("precision:   %0.3f" % pre_score)
    print("AUC:   %0.3f" % auc_score)

    if hasattr(clf, 'coef_'):
        print("dimensionality: %d" % clf.coef_.shape[1])
        print("density: %f" % density(clf.coef_))

        print()

    print("confusion matrix:")
    print(metrics.confusion_matrix(y_test, pred))

    print()
    clf_descr = str(clf).split('(')[0]
    return clf, clf_descr, acc_score, pre_score, auc_score, train_time, test_time



def res_to_df(res):
    colnames = ["clf", "clf_descr", "acc_score", "pre_score", "auc_score",
                "train_time", "test_time"]
    res = pd.DataFrame.from_dict(res)
    res.columns = colnames

    return res


def cv(X,y, clf):
    '''
    Cross validation for a classifier (specify classifier in the function body).
    Returns accuracy, precision, AUROC, classifier information, train time, test time.

    Example clf
        RidgeClassifier(tol=1e-2, solver="lsqr")
        # linearSVC with l2 pen
        LinearSVC(loss='l2', penalty='l2', dual=False, tol=1e-3)
        # bernoulliNB
        BernoulliNB(alpha=.01)
        KNeighborsClassifier(n_neighbors=5, weights='distance', metric='cosine', algorithm='brute')
    '''
    cv = StratifiedKFold(n_splits=10)

    results = []
    for train, test in cv.split(X, y):
        results.append(
            benchmark(clf, csr_matrix(X[train]), csr_matrix(X[test]), y[train], y[test])
                    )

    return res_to_df(results)

def cv_multiclass(X, Y, clf):
    '''
    Cross validation for a multiclass classifier
    (Y is a matrix not a vector)
    '''
    results = {}
    for kw in Y.columns:
        try:
            results[kw] = cv(X, Y[kw], clf)
        except ValueError as e:
            print(e)

    results = pd.concat(results)
    results.reset_index(inplace=True)
    results.drop('level_1', axis=1, inplace=True)
    results.rename(columns={'level_0':'keyword'}, inplace=True)
    return results.groupby('keyword').mean()


