#!/usr/bin/env python3

import argparse
import pandas as pd
import numpy as np
import sys
import os

from skbio.stats.composition import ancom, multiplicative_replacement
from sklearn.decomposition import PCA
from itertools import combinations
from skrzynka import delete_empty, dummy_df


N_JOBS = 6
resdir = "differential_analysis_results_py/"
tempdir = resdir+"tempdir/"
if not os.path.exists(resdir):
    os.mkdir(resdir)
if not os.path.exists(tempdir):
    os.mkdir(tempdir)


def design_matrix(md, variable_colnames):
    return pd.get_dummies(md[variable_colnames])




def subset_sam_md(sam_md, min_samples_per_group, max_samples_per_group,
                    contrast_colname='group_word_count'):
    '''
    All groups below min_samples_per_group will be removed
    All groups above max_samples_per_group will be subsetted
    '''

    #new_md = sam_md.groupby(contrast_colname)\
    #            .apply(lambda x: subset_sam_md_(x,
    #                            min_samples_per_group, max_samples_per_group)
    #                    )
    ma = max_samples_per_group
    mi = min_samples_per_group

    new_md = sam_md.groupby(contrast_colname)\
                .apply(lambda x: x if x.shape[0] > mi else None)
    new_md = new_md.groupby(contrast_colname)\
                .apply(lambda x: x.ix[:ma,:] if x.shape[0] > ma else None)

    new_md.index = new_md['sample_id']
    return new_md


def diffab_matrix_from_deseq(deseq_results):
    DA = delete_empty(
            pd.DataFrame.from_dict({k:v['padj'] for k,v in deseq_results.items()})
        )
    return DA

##########################
# Use scikit-bio and ancom
##########################


#table = dummy_df()

def log2fc(A, grouping):
    '''
    first group is a reference (0)
    for two conditions for now
    '''
    groups = set(grouping)
    mat, cats = A.align(grouping, axis=0, join='inner')
    #A[grouping == groups[0]]
    G1, G2 = [i[1] for i in mat.groupby(cats)]
    G1 = G1.mean()
    #for var in G2.columns:
    G2 = G2.mean()
    FC = G1/G2
    FC[[i == 0 or i == np.inf for i in FC]] = np.nan
    logFC = np.log2(FC)
    return logFC



def ancom_preprocessing(A):
    #return A.applymap(lambda x: x+1)
    #A =
    return multiplicative_replacement(A)

def da_ancom(A, grouping, **kwargs):
    if isinstance(grouping, list):
        grouping = pd.Series(grouping, index=A.index)
    elif isinstance(grouping, dict):
        grouping = groups(grouping)
    elif isinstance(grouping, pd.Series):
        pass

    A = ancom_preprocessing(A)
    da = ancom(A, grouping, **kwargs)
    return da[0]



def subset_biome(A, biome):
    pass

def groups():
    groups_dict = {     'group1': [ 'SRS534346', 'SRS480767', 'ERS951177'],
                        'group2': [ 'SRS879376', 'SRS590878', 'ERS916180', 'SRS836937']
                }
    groups = {}

    for k,v in groups_dict.items():
        for v_ in v: #sample ID
            groups[v_] = k

    return pd.Series(groups)





