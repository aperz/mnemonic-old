#!/usr/bin/env python2

'''
'''

import argparse
import csv
import os
import sys
import urllib
import io
#from urllib2 import URLError #py3
import requests
from lxml import html
import pandas as pd

def integrate_information(run2prj_fp):
    sam = pd.read_csv(odir_path + "all_samples.csv", error_bad_lines=False)
    prj = pd.read_csv(odir_path + "all_projects.csv", error_bad_lines=False)

    run2prj = pd.read_csv(run2prj_fp, sep="\t", error_bad_lines=False)

    # Integrate information
    prj.columns = [i.replace(" ", "_").lower() for i in prj.columns]
    sam.columns = [i.replace(" ", "_").lower() for i in sam.columns]
    run2prj.columns = [i.replace(" ", "_").lower() for i in run2prj.columns]

    sam.drop("unnamed:_4", axis = 1, inplace = True)
    prj.rename(columns={"study_id":"project_id"}, inplace=True)
    #sam.to_csv("test/sam.tsv", sep='\t')
    #prj.to_csv("test/prj.tsv", sep='\t')
    #run2prj.to_csv("test/run2prj.tsv", sep='\t')

    whole = pd.merge(sam, run2prj, on="sample_id", how="inner")
    #whole = pd.merge(prj, whole, on=["project_id", "project_name"], how="inner")
    whole = pd.merge(prj, whole, on="project_id", how="inner")

    #print(whole.columns)

    #col_seq = ['biome', 'sample_id', 'sample_name', 'project_name', 'project_id',
    #'run_id', 'classification', 'experiment_type', 'experimental_feature',
    #'material', 'envo', 'uberon', 'sample_description', 'number_of_samples',
    #'submitted_date', 'analysis', 'ncbi_project_id', 'public_release_date',
    #'centre_name', 'experimental_factor', 'is_public', 'study_linkout',
    #'study_abstract']
    #whole = whole[col_seq]

    return whole


def get_list_of_projects(url):
    """
    Returns a list of all unique project IDs in a .csv file
    specified in 'url'.
    """
    s = requests.get(url).content
    projects = pd.read_csv(io.StringIO(s.decode('utf-8')), sep=",", error_bad_lines = False)

    if "Study ID" in projects.columns:
        return projects\
            ["Study ID"]\
            .unique()\
            .tolist()
    else:
        sys.exit("Something's wrong; is the EBI MG project list URL still active and still contains column named 'Study ID'?")


def get_list_of_samples(url):
    """
    Returns a list of all unique sample IDs in a .csv file
    specified in 'url'.
    """
    s = requests.get(url).content
    samples = pd.read_csv(io.StringIO(s.decode('utf-8')), sep=",", error_bad_lines = False)

    if "Sample ID" in samples.columns:
        return samples\
            ["Sample ID"]\
            .unique()\
            .tolist()
    else:
        sys.exit("Something's wrong; is the EBI MG project list URL still active and still contains column named 'Study ID'?")


def _get_file_stream_handler(url_template, project_id):
    """
    Returns a file stream handler for the given URL.
    Get a url to retrieve the list of runs for a study from
    """
    url_get_project_runs = url_template % (project_id)
    try:
        print(url_get_project_runs)
        return urllib.urlopen(url_get_project_runs)
    #except URLError as url_error:
    #    print(url_error)
    #    raise
    except  IOError as io_error:
        print(io_error)
        raise
    except ValueError as e:
        print(e)
        print("---Could not retrieve any runs. Open the retrieval URL further down in your browser and see if you get any results back. Program will exit now.")
        print(url_get_project_runs)
        raise


def get_run2prj(run2prj_fp, study_url_template, project_information_url):
    """
    Saves a table with run to project mapping for a given project list
    to run2prj_fp
    """
    #TODO get from the run table from ebi!!

    # manage files, erase the previous content
    if not os.path.exists(run2prj_fp):
        os.mknod(run2prj_fp)
    open(run2prj_fp, 'w').close()

    # get the projects list
    list_of_projects = sorted(get_list_of_projects(project_information_url))
    print("---Getting mapping for "+str(len(list_of_projects))+" projects")

    total_n_runs = 0
    errors = 0

    with open(run2prj_fp, 'a') as ofile:
        header = "project_id\tsample_id\trun_id\n"
        ofile.write(header)

        for project_id in list_of_projects:
            # Retrieve a file stream handler from the given URL and iterate over each line (each run) and build the download link using the variables from above
            file_stream_handler = _get_file_stream_handler(study_url_template, project_id)
            #py3: reader = unicodecsv.reader(file_stream_handler, delimiter=',', encoding = 'utf-8')
            reader = csv.reader(file_stream_handler, delimiter=',')

            try:
                for project_id, sample_id, run_id in reader:
                    total_n_runs +=1
                    # Write line to output file
                    new_line = "\t".join([project_id, sample_id, run_id]) + "\n"
                    new_line = new_line.encode("utf-8")
                    ofile.write(new_line)
            except ValueError as e:
                print("ValueError:")
                print(e)
                print(project_id, sample_id, run_id)
                errors +=1
                pass

    return total_n_runs, errors

def clean_metadata(md):
    return md.ix[md['sample_id'].str.contains("RS"),:]


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="A tool to retrieve project information, sample information and\
                        run IDs for all EBI metagenomics projects.\
                        WARNING: Some samples have multiple runs and this\
                        is relfected in the url (but in only 13 of all projects, e.g. ERP001227: \
                        https://www.ebi.ac.uk/metagenomics/projects/ERP001227/samples/ERS095015/runs/ERR091537,ERR091538,ERR091539,ERR091540,ERR091541,ERR091542,ERR091543,ERR091544/results/versions/1.0)\
                        These are excluded. Fix the csv reader part if interested in getting those.\
                        ")
    parser.add_argument("-o", "--output_path",
                        help="Location of the output directory (folder!), where the files get stored.**MANDATORY**",
                        required=True)
    parser.add_argument("-a", "--action",
                        help="'only_download' or 'only_integrate'; optional",
                        default = "download_and_itegrate",
                        required=False)
    args = vars(parser.parse_args())

    odir_path = args['output_path']+'/'
    ofile_path = odir_path + "/full_metadata.tsv"
    run2prj_fp = odir_path + "/run2prj.tsv"
    action = args['action']

    root_url = "https://www.ebi.ac.uk"
    study_url_template = root_url + "/metagenomics/projects/%s/runs"

    sample_information_url = "https://www.ebi.ac.uk/metagenomics/samples/doExportTable?searchTerm=&sampleVisibility=ALL_PUBLISHED_SAMPLES&search=Search&startPosition=0"
    project_information_url = "https://www.ebi.ac.uk/metagenomics/projects/doExportDetails?search=Search&studyVisibility=ALL_PUBLISHED_PROJECTS"

    # manage dirs and erase the previous content of the output file
    if not os.path.exists(odir_path):
        os.mkdir(odir_path)
    if not os.path.exists(ofile_path):
        os.mknod(ofile_path)
    open(ofile_path, 'w').close()

    if not action == "only_integrate":
        urllib.URLopener().retrieve(sample_information_url, odir_path + "all_samples.csv")
        urllib.URLopener().retrieve(project_information_url, odir_path + "all_projects.csv")

        total_n_runs, errors = get_run2prj(run2prj_fp, study_url_template, project_information_url)
        print("--- There were "+str(total_n_runs)+" total runs across all projects.")
        print("--- There were "+str(errors)+" ValueErrors")

    if not action == "only_download":
        print("--- Integrating information")
        whole_table = integrate_information(run2prj_fp)

        print("--- Cleaning")

        #ATT #cleaning loses a little data (like, 30 samples)
        whole_table = clean_metadata(whole_table)
        whole_table.to_csv(ofile_path, sep="\t", index=False)

    print("--- Program finished.")


