#! /usr/bin/env bash

# folder names w/o the slash
input_fp=$1/ #dir 
output_fp=$2/ #dir 
metadata_fp=$3 #dir

mkdir $output_fp
cd $input_fp

for file in $(ls | grep RP); do
    #echo $file"_metadata.biom"
    cmd="biom add-metadata -i "$input_fp$file" -o "$output_fp$file"_metadata.biom -m "$metadata_fp 
    echo $cmd
    eval $cmd
done


