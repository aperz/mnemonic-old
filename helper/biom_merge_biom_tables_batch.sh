#! /usr/bin/env bash

### NOT TESTED
# folder names w/o the slash
input_fp=$1 #dir 
output_fp=$2/ #dir 


mkdir $output_fp
cd $input_fp

echo $(ls | grep RP | wc -l)

function from_raw_to_projects {
    echo "Merging from the raw download to files corresponding to projects"
    # for project in list_projects / /for folder in ls 
    for folder in $(ls | grep RP); do
        echo $folder
        # go into dir 
        cd $folder/OTU-BIOM/

        # merge all files that match an expression
        tables_list="$(ls -m | grep RR | grep BIOM | tr -d "\n")"
        cmd="merge_otu_tables.py -i "${tables_list//[[:blank:]]/}" -o "$output_fp$folder"_OTU-BIOM.biom"
        eval $cmd
        cd ../..
    done
}


function from_projects_to_one_file {
    cd $output_fp
    echo "Merging projects into single file"

    # merge all files that match an expression
    tables_list="$(ls -m | grep RP | grep BIOM | tr -d "\n")" #ATT matches projects
    cmd="merge_otu_tables.py -i "${tables_list//[[:blank:]]/}" -o OTU-BIOM_all.biom"
    eval $cmd
}


from_raw_to_projects
#from_projects_to_one_file

# add metadata
#validate_mapping_file.py -m /dat1/metag/data/differential_abundance/metadata_biom_ATT.tsv -b -p -o ../differential_abundance/biom_map_ATT -s
#biom add-metadata -i /dat1/metag/data/OTU-BIOM/merged/ERP001506_OTU-BIOM.biom -o /dat1/metag/data/OTU-BIOM/ERP001506_md.biom -m /dat1/metag/data/differential_abundance/biom_map_ATT/metadata_biom_ATT.tsv_corrected.txt

