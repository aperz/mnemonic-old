#! /usr/bin/env bash

# folder names w/o the slash
input_fp=$1 #dir 
output_fp=$2/ #dir 

mkdir $output_fp
cd $input_fp

for file in $(ls | grep RR); do
    #echo $file"_metadata.biom"
    cmd="biom convert -i "$input_fp$file" -o "$output_fp$file"_json.biom --to-json"
    echo $cmd
    eval $cmd
done


