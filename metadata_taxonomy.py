#!/usr/bin/env python3

description = '''
Split the "taxonomy" column from EBI data into separate columns for
each taxonomical level.
'''
description

import argparse
import pandas as pd
from skrzynka import mapp
import os
import subprocess as sp

#taxonomy_ids_url = "ftp://greengenes.microbio.me/greengenes_release/gg_13_5/gg_13_5_taxonomy.txt.gz"
taxonomy_ids_url = "ftp://greengenes.microbio.me/greengenes_release/gg_13_8_otus/taxonomy/99_otu_taxonomy.txt"



def get_tax_tree():
    pass


def taxonomy_version_to_new(tax_list):
    '''
    In some versions of taxonomy there are no spaces between the taxonomic
    levels. Fix that, add spaces.
    '''
    return ["; ".join([i for i in j.split(";")]) for j in tax_list]

def get_greengenes_ids(taxonomy_ids_url):
    filename = taxonomy_ids_url.split('/')[-1]
    sp.call('wget '+taxonomy_ids_url, shell=True)
    ids = pd.read_csv(filename, sep='\t', header=None)
    ids.columns = ['taxon_id', 'taxonomy']
    ids.drop_duplicates()
    os.remove(filename)
    return ids



def taxonomy_metadata_save(ifile_path, ofile_path):
    #taxonomy = pd.read_csv(ifile_path, sep="\t")['taxonomy']\
    #            .drop_duplicates().tolist()
    gg = get_greengenes_ids(taxonomy_ids_url)

    open(ofile_path, 'w').close()

    with open(ofile_path, "a") as ofile:
        ofile.write(
            "taxon_id\ttaxonomy\tkingdom\tphylum\tclass\torder\tfamily\tgenus\tspecies\n"
            )

        for _,t in gg.iterrows():
            try:
                tax = [i[3:] for i in t[1].split("; ")]
                ofile.write(str(t[0])+"\t"+t[1]+"\t"+"\t".join(tax)+"\n")

            except AttributeError:
                # some nans ><
                print(t)

    print("Saved to", ofile_path)

def collapse_by_taxonomic_level(A, tax_md, by='family'):
    '''
    Collapse columns of a matrix by a taxonomic level
    '''
    # transpose, seems easier to work with index
    A = A.T
    mapping = mapp(tax_md, 'taxonomy', [by])
    A = pd.merge(A, mapping, how='left', right_index=True, left_index=True)
    A = A.groupby(by).sum()

    return A.T




if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("-i", "--input_path",
                        required=True)
    parser.add_argument("-o", "--output_path",
                        help="Path to the output .tsv file with taxonomy split into separate columns",
                        required=True)
    args = vars(parser.parse_args())

    ifile_path = args['input_path']
    ofile_path = args['output_path']

    taxonomy_metadata_save(ifile_path, ofile_path)


