'''
Preprocessing-related things / wrappers for pandas DataFrames

https://www.r-bloggers.com/do-not-log-transform-count-data-bitches/
"For count data, our results suggest that transformations perform poorly. An additional problem with regression of transformed variables is that it can lead to impossible predictions, such as negative numbers of individuals. Instead statistical procedures designed to deal with counts should be used, i.e. methods for fitting Poisson or negative binomial models to data. The development of statistical and computational methods over the last 40 years has made it easier to fit these sorts of models, and the procedures for doing this are available in any serious statistics package."
'''

import pandas as pd
import numpy as np
from math import log
from sklearn.preprocessing import scale, normalize, quantile_transform
from sklearn.preprocessing import robust_scale
from sklearn.preprocessing import maxabs_scale
#from skleran.preprocessing import *

#TODO no tensorflow for python3.6 yet
#from fancyimpute import BiScaler

from scipy.spatial import distance_matrix


def binarize_count_data(A):
    return A.applymap(lambda x: 1 if x > 0 else 0)


def binarize_pval(A, threshold, invert=False):
    o = A.applymap(lambda x: x < threshold)

    if invert:
        o = o.applymap(lambda x: not x)

    o = o.applymap(lambda x: int(x))

    return o



def correspondence_analysis_transform(A):
    print("May be implemented incorrectly")
    N = A.sum(0).sum()
    A_expected = pd.DataFrame(
        np.outer(A.apply(sum, 1), A.apply(sum, 0)),
        columns = A.columns, index = A.index
        )\
        .applymap(lambda x: x / N )\

    A_regularised = A * A_expected / A_expected**(1/2)

    return(A_regularised)

def normalize_pd(d, **kwargs):
    return pd.DataFrame(normalize(d, **kwargs),
                        columns = d.columns, index = d.index)

def scale_pd(d, **kwargs):
    return pd.DataFrame(scale(d, **kwargs),
                        columns = d.columns, index = d.index)

def maxabs_scale_pd(d, **kwargs):
    return pd.DataFrame(maxabs_scale(d, **kwargs),
                        columns = d.columns, index = d.index)

def quantile_transform_pd(d, **kwargs):
    '''
    Transforms FEATURES, so columns.
    '''
    return pd.DataFrame(quantile_transform(d, **kwargs),
                        columns = d.columns, index = d.index)

def mean_center_pd(d, axis=1):
    return d.apply(lambda x: x-x.mean())

def normalise_my(x):
    return (x-min(x))/(max(x)-min(x))

def composition_pd(d):
    if hasattr(d, 'shape') & len(d.shape) == 2:
            return d.apply(lambda v: [i/sum(v) for i in v])
    else:
        return [i/sum(d) for i in d]

def negative_binomial_transform():
    return 'ni'


def scale_sklearn(A, axis=0, method="scale"):
    '''
    axis:       0: scale rows; 1: columns; 2:scale both two times (TODO: until <epsilon)
    '''
    print('See vacou scaling etc and remove *this* bs')
    if method == "scale":
        if axis == "both":
            o = scale(scale(scale(scale(A, 0),1),0),1)
            o = scale(scale(scale(scale(A, 0),1),0),1)
            o = scale(scale(scale(scale(A, 0),1),0),1)
            o = scale(scale(scale(scale(A, 0),1),0),1)
        else:
            o = scale(A, axis)
    if method == "maxabs_scale":
        if axis == "both":
            o = maxabs_scale(maxabs_scale(maxabs_scale(maxabs_scale(A, 0),1),0),1)
            o = maxabs_scale(maxabs_scale(maxabs_scale(maxabs_scale(A, 0),1),0),1)
        else:
            o = maxabs_scale(A, axis)
    if method == "robust_scale":
        if axis == "both":
            o = robust_scale(robust_scale(robust_scale(robust_scale(A, 0),1),0),1)
            o = robust_scale(robust_scale(robust_scale(robust_scale(A, 0),1),0),1)
        else:
            o = robust_scale(A, axis)

    o = pd.DataFrame(o, columns = A.columns, index = A.index)

    return o

def scale_fancyimpute(A):
    '''
    Biscaling using BiScaler function from fancyimpute.
    '''
    bs = BiScaler()
    o = bs.fit_transform(np.array(A))
    o = pd.DataFrame(o, columns = A.columns, index = A.index)
    return o


def log_transform_counts(A, base=2):
    '''
    Perform log transform on a count matrix
    leave zeros unchanged
    Valid? not really - count 1 is different than 0: there is an important
    difference between a taxon not present and present in low amounts.
    '''
    #Ao = pd.DataFrame(
    #    np.log(A),
    #    columns = A.columns, index = A.index
    #    )
    return A.applymap(lambda x: np.nan if x==0 else log(x, base))
    #return A.applymap(lambda x: 0 if x==0 else log(x))

def filter_low_counts(A, row_min=None, col_min=None):
    '''
    For EBI abundance data does it even make sense?
    Remove taxons/samples with the lowest count sums.
    '''
    A = A[A.sum > row_min, A > col_min]
    pass
