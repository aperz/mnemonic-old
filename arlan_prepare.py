#!/usr/bin/env python3

import pandas as pd
from metadata_taxonomy import taxonomy_version_to_new

data_ifp = '/D/Arlan/MicrobiomeDataOldYoungCR_AL.xlsx'
data_ofp = '/D/Arlan/arlan_data.tsv'
metadata_ofp = '/D/Arlan/arlan_sample_metadata.tsv'

cr = pd.read_excel('/D/Arlan/MicrobiomeDataOldYoungCR_AL.xlsx', header=0)

new_cols=[]
for c in cr.columns:
    if c.find('Unnamed') == -1:
        current = c
        new_cols.append(current)
    else:
        new_cols.append(current)
cr.columns = new_cols

cr = cr.T
cr.rename(columns={'Taxon':'sample_id'}, inplace=True)
md = pd.DataFrame(cr.reset_index().ix[:, 0:2].values, columns = ['group', 'sample_id'])
md.group = [i.strip() for i in md.group]
md['project_id'] = 'arlan'

cr.index = cr['sample_id']
data = cr.ix[:, 2:]
data.columns = taxonomy_version_to_new(data.columns)

data.to_csv(data_ofp, sep='\t')
md.to_csv(metadata_ofp, sep='\t')
