# mnemonic project coordination and progress centre
---
---


# ROADMAP
## Primary conceptual goals:
1. [ ] Be able to provide a researcher with a list of metagenomics publications / projects
that are similar to theirs wrt. taxonomic abundance.
1. [ ] Be able to provide a researcher with a list of conditions
that are similar to theirs wrt. taxonomic abundance.


## Primary specific goals:
1. [ ] Develop a tool that can be used by dr Wren. Input: a query sample (set of samples),
output: samples most similar to query wrt. taxonomic abundance.
1. [ ] Develop a tool that can be used by dr Wren. Input: a microorganism,
output: which microorganisms are seen often with the query microorganism.
(#global: taxons for each biome)
1. [ ] Find a biological interpretation of data (a story). See ideas below
(#focus-samples, #story)  
OR  
Make some statements about the general results. A PCA plot of samples
(annotated with condition / biome) or microbes would be central
to this goal.


## Secondary specific goals:
1. [ ] Tool. Shift similarity.
E.g. is the shift between healthy and diabetic similar to a shift between
healthy and antibiotic-treated?
An important step to achieveing this goal is to
annotate samples with Experimental Group (control - disease etc.).
1. [ ] Tool.
Output: which conditions are enriched with which GO term?
(Potentially later: with which pathway?)
Personally, I am interested in functional analysis.
I also know a person that might be interested in that from Pat Gaffney lab.
1. [ ] Be able to predict condition annotation of samples.
1. [ ] Be able to determine which taxons constitute a core microbiome of a biome / condition



---
---
# RANDOM OTHER IDEAS
The below content serves a purpose of a notebook.


## WEBAPP
THE WEBAPP GOALS ARE LISTED AS ISSUES ON THE PROJECT SITE.
- [x] md - query metadata (quick manual lookup)
- [x] md - stats: taxonomy, sample
- [x] md - stats: biome counts
- [x] A - plots: dist of raw / transformed counts
- [x] md, D - query similar samples with a sample ID
- [ ] submit data as tsv - try Arlan's data
- [x] md, D - query similar taxons
- [x] add a link from a project ID to EBI website
- [ ] add a ProTraits list for each taxon
- [x] differential abundance for a group of samples (predefined groups?): results: heatmap and tree-structure plot thing (subset to low pval)
- [ ] define and browse core microbiomes - predefine a few core microbiomes - add core microbiomes as entities
- [ ] add n_background as a metric for taxons
- [ ] similar shifts?
- [ ] more metadata stats: % of human / mouse ("habitat sampling"), % of body sites
- [ ] functional changes - treat function as another entity
- [ ] add mgrast metadata like the amgut - just for exploration purposes (doesn't probably contain any addiional data on EBI samples)
- [ ] for CONTRASTS, add a number of overlapping samples (a Venn diagram preferably)


## TOOL

### differential abundance
- [ ] diffabu func that takes a dict {'condition1':['list', 'of', 'sampleIDs'], 'condition2':[ ... 

- vs. 'core microbiome': OUTPUT: p-val, logFC - plot
----|               - at genera level or something, and [:10] or where p-val < 0.05
  --|               - core is reference
    |--
    |-

### global: core microbiome
- [ ] get 'healthy' samples fo human gut
                    

### pairwise:
- [ ] add metrics: chi2 distance

### global: taxons for each biome
    see ebi_summary.ipynb for table
- [ ] shifts between conditions! (need more # METADATA)
- [ ] coinhabitance: which taxons belong together? Which are specific to a certain condition, in oher words, are enriched compared to other samples?
> (ONLY USE the samples that CONTAIN this microbe to assess siginificance / enrichment!)
- [ ] antiinhabitance: which taxons appear in similar conditions, but not often together?


### query taxon specific, e.g. C. difficile - biological questions
- [ ] which conditions does it prevail in?
- [ ] which other microbes are specific to these conditions?



### query: for any query entities
- [ ] add metrics: chi-square, (n_overlapping, fisher,...)

### query: for query samples
- [x] run on functional data?
- [x] Get output for the # focus samples
    - get a list of similar samples
    - get a list of similar projects (weighted? 1/(n_samples in project)); also get paper link list?
- [x] get diff otus (within query: as a bonus, and also between query and closest samples)


## METADATA: extraction from text
### 'ANNOTATIONS': determine treatments / conditions across projects
- [ ] how non-unique are they? Is it possible to pool / compare them across projects? 
- [ ] keywords_matched is a sketch version of those
    - biome (easy but broad)
    - disease
- [ ] unsupervised clustering within a biome? (circular)
- [ ] age, location, envo, extraction - Cory (trie matcher is in wrenlab) - perhaps use EXTRACT?

### TRT extraction: group into trt by:
- [ ] word count for all MD cols - which col is most variable?
    - only sample_name and keywords provided any information
    - try gather more data (scraping? ENA?)
- [ ] string distances?
- [ ] clustering based on abundance data
> so far not very promising
    - try diff abundance methods?

### TRT extraction: posotive control: manually look into those:
- [ ] that have data from ENA that distinguishes samples in prj
- [ ] with experimental_factor values in project data
- [ ] that are reasonably_grouped

### ontologies?
- [x] available tool to annotate with ontologies? (biome / sample source is most important)
- [x] sample source ontology
> direct hits for ENVO: 9900 samples
> no direct hits for DOID, BTO


## METADATA: more info
- [x] use run to sam / prj mapping from https://www.ebi.ac.uk/metagenomics/search
- [x] get info on envo / oberon (ebi.ac.uk/ena/data/view/ERS|ERR)
    - ena_sample_info_get.py
> v. detailed (but non-generalizeable) sample data
> envo: about 612/50000 samples (little!)
> experiment_type: ~3/5 runs have that info
> condition?
- [ ] missing info: sample_classification
- [ ] which databases have info on my samples?
> ENA, GEO?, SRA?, ...
- [ ] can I extract age from metadata of human samples? - CORY
> MAKE IT SO THAT I CAN FEED THE MD TO WRENLAB
- [ ] add alpha and beta div as variables? (that should be implicit in the model but is it?)
- [x] get ENA metadata
- [x] get MG-RAST metadata
- [ ] get ProTraits data


## VALIDATION / EVALUATION
### exploratory / sanity checks
    ebi_summary.py
- [x] compare .biom and .tsv
> matching!
- [ ] alpha, beta diversity distributions
- [ ] kNN / clustering with n=n_projects: compare to actual projects
- [ ] clustering into biomes and compare vs actual biomes
- [ ] PCA plot R > plotPCA
- [ ] MA plot?: R > plotMA
- [x] mean distance
- [x] Mantel fun vs. tax: get full functional data matrix. calculate sample-sample relationships for fun and tax. Compare with Mantel (validation.ipynb).
- [ ] manually: look up
- [ ] use wrenlab! - send dr Wren list of taxons to look up with IRIDESCENT
- [ ] @minter would be similar?
- [ ] Henshel
- [ ] things similar MPLasso (MPLasso has little info but perhaps)
- [ ] what explains the differences best? (except pid)




## TRANSFORMATION AND PREPROCESSING
- [ ] optimisation pipeline: agains the validation tests (Mantel fun vs. tax, mean distances)
    - optimise processing steps and distance method
- [ ] Mantel implementation for non-distance?
- [ ] also test on some document classification dataset / simulated dataset? (similar data structure but readily available truth)

### Methods
    In one of the notebooks there's a list of methods with justifications.
- [ ] revisit corana implementation
- [ ] cut off low abundance taxons (Cory is right that count 1 might easily be an artifact)
- [x] tf-if + cosine
- [ ] quantile normalization [Bulland JM, BMC Bioinf, 2010]
- [ ] glms? Rigde, linear SVM (SVC), SGD (sklearn)
> I tested those on a classification problem with my data (on keywords), results seem uninformative (probably because keywords suck). See /P/glm
- [x] how unusual are the relationships?: Fisher's exact
- [x] how unusual are the relationships?: number of overlapping




## 'FOCUS' SAMPLES
- Arlan's data
> Nothing associated with cal restr shows much as of 28 March.
> obesity: 1700 samples
> diet: 30 projects
- Bergstrom: data on gut microbe* interactions*
    - control-diseased
    - break down by different vriables -> groups
- mouse: Umesh, Brian: get mouse experiments
- C. difficile ('is cdf enriched in...')
    - colitis, diarrhea, CFI
> max. 100 (c. diff) + 1040 (diarrhea)
- diabetes? (many people are studying diabetes, find someone)
> 8400 samples (4 prjs) w diabetes + possibly in AmGut.subset_diabetes
- Alzheimer's
- mental / mood disorders: depression, major depressive disorder (MMD), anxiety, bipolar disorder (BD), obsessive-compulsive (OCD) / gut-brain axis, 
- irritable bowel syndrome (IBS IBD), GI infection
- AMERICAN GUT METADATA CAN CONTAIN MORE INFORMATION!! (autism etc)
- pharmacuticals? (American Assiciaion of Pharmaceutical..., MCBIOS)
> mouse screening? Prob not on metagenomics
- Robert: SLE. KEGG. Decrease in alpha-div
- there's a LOT of age data (especially for AmGut)
- Micah: dental plaque
- !! can I model spatial change : passing through intestine? (core large intestine etc: watch out for fecal samples)

## PROMISING:
- age
- diabetes

## STORY
- for conditions [] what microbes are enriched vs. healthy (as defined by AmGut subset_healthy)


## IDEAS
- core microbiome? (for biome / condition (trt))
- time-series (see MetaMIS, lotka-voilterra model) - how many samples can I extract this information from?
- a map of where the samples are from - how often is location specified in metadata
- ENRICHMENT: ProTraits, functions. A simple approach: Take a mean of traits



## NOTES
- Carl Cerniglia (MCBIOS): gut-brain axis. MACS - microb.-assiciated carbs
- microbiome affects drug metabolism? (cit)
- http://metagenomesonline.org/ another database (53 projects; seems like only aquatic)
MgOl libraries are annotated with an abundance of sample metadata including sample provenance, geographical description, environmental parameters,
sampling and preparation methodologies, and Environmental Ontology (ENVO) terms



---
# WHAT I'VE DONE METAGENOMICS (SCILOG)

Thu Sep 21 16:13:40 CDT 2017
- downloaded metadata for mgrast
- made the skrzynka package installable

Wed Sep 20 
- added new metrics: fisher exact, chisquare kernel, n_overlapping

Mon Mar  7 17:03:08 CST 2016
- metagenomics: gave dr Wren metadata and taxonomy data
- got comparision of top correlated transcripts working

