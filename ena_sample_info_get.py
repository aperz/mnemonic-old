#!/usr/bin/env python3

'''
A description is available when running with the -h argument
in the command line.
'''


import pandas as pd
from xml.etree import ElementTree as et
import argparse
import requests
import io
import os


def xml2data(xml_root):
    attribute_names = []
    attribute_values = []
    for c in xml_root.find('SAMPLE').find('SAMPLE_ATTRIBUTES')\
        .findall('SAMPLE_ATTRIBUTE'):
            attribute_names.append(c.getchildren()[0].text)
            try:
                attribute_values.append(c.getchildren()[1].text)
            except IndexError as e:
                print(e)
                attribute_values.append("NA")

    assert len(attribute_names) == len(attribute_values)

    data = pd.DataFrame({
        'attribute_name': attribute_names,
        'attribute_value': attribute_values
        })

    return data


def process_all(use_samples, ena_sample_info_url_template, parsed_fp):
    #clear output file and write header
    with open(parsed_fp, 'w') as f:
        f.write(header)
        f.close()

    with open(parsed_fp, "a") as f:
        for s in use_samples:
            # append forming a "long" data table; less elegant, more robust
            # to different numbers of SAMPLE_ATTRIBUTEs
            #xml_root = et.parse(xml_file) # if parsing from file
            ena_sample_info_url = ena_sample_info_url_template.format(sample_id = s)
            print(ena_sample_info_url)

            #xml_content = requests.get(ena_sample_info_url).content
            try:
                xml_root = et.fromstring(
                    requests.get(ena_sample_info_url).content
                    )

                try:
                    data_for_project = xml2data(xml_root)
                    data_for_project\
                        .insert(loc=0, column="sample_id", value=[s for i in range(data_for_project.shape[0])])
                    data_for_project.to_csv(f, header=False, sep="\t", index=False)
                except AttributeError as e:
                    print(e)
                    print("There was a problem with", s)

            except xml.etree.ElementTree.ParseError as e:
                print(e)
                print("There was a problem with", s)


def get_list_of_samples(url):
    """
    Returns a list of all unique sample IDs in a .csv file
    specified in 'url'.
    """
    s = requests.get(url).content
    samples = pd.read_csv(io.StringIO(s.decode('utf-8')), error_bad_lines=False)

    if "Sample ID" in samples.columns:
        return samples\
            ["Sample ID"]\
            .unique()\
            .tolist()
    else:
        sys.exit("Something's wrong; is the EBI MG project list URL still active and still contains column named 'Study ID'?")

def reshape_table(parsed_fp):
    parsed = pd.read_csv(parsed_fp, sep="\t")
    indx = parsed.sample_id.unique()
    cols = parsed.attribute_name.unique()
    full_data = pd.DataFrame(columns = cols)

    for n, s in parsed.groupby("sample_id"):
        gre = s.to_dict("record")
        gre = {i['attribute_name']: i['attribute_value'] for i in gre}
        gre = {i:(gre[i] if i in gre.keys() else "NA") for i in cols}
        gre = pd.DataFrame(gre, index = [n], columns = cols)
        full_data = pd.concat([full_data, gre])

    return full_data


#def american_gut_md():
#    call = 'wget ftp://ftp.microbio.me/AmericanGut/ag-July-29-2016/04-meta/ag-cleaned.txt'



def last_modifications(md):
#TODO Unknown -> Null
    md.applymap(lambda x: None if x == 'Unknown' else x)
    md.rename(columns={
                'SAMPLE_IDENTIFIERS_PRIMARY_ID': 'sample_id'
                })



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Retrieve sample info from ENA")
    parser.add_argument("-o", "--odir_path",
                        help="",
                        required=True)
    args = vars(parser.parse_args())
    odir_path = args['odir_path']

    parsed_fp = odir_path+"/parsed.tsv"
    output_fp = odir_path+"/output.tsv"
    #parsed_fp = "/dat1/metag/data/ENA_xml_parsing/parsed.tsv"

    if not os.path.exists(odir_path):
        os.mkdir(odir_path)
    if not os.path.exists(output_fp):
        os.mknod(output_fp)
    if not os.path.exists(parsed_fp):
        os.mknod(parsed_fp)

    all_sample_info_url = "https://www.ebi.ac.uk/metagenomics/samples/doExportTable?searchTerm=&sampleVisibility=ALL_PUBLISHED_SAMPLES&search=Search&startPosition=0"
    ena_sample_info_url_template = "https://www.ebi.ac.uk/ena/data/view/{sample_id}&display=xml"
    header = "sample_id\tattribute_name\tattribute_value\n"


    use_samples = get_list_of_samples(all_sample_info_url)
    #use_samples = ["ERS914852", "SRS659217"]
    process_all(use_samples, ena_sample_info_url_template, parsed_fp)
    reshape_table(parsed_fp).to_csv(output_fp, sep="\t")
    print(output_fp)

