#!/usr/bin/env python3
# coding: utf-8

import argparse
import pandas as pd
import numpy as np
import sys
import os

from sklearn.decomposition import PCA
from itertools import combinations
import skrzynka as sk
from data_management import get_sam_md, get_tax_md, get_go_md, AMGUT_MD_FP
import preprocess

from data_management import *

# rpy2
from rpy2.robjects import r
from rpy2.robjects import pandas2ri
from rpy2.robjects.packages import importr

from rpy2.robjects.conversion import localconverter
from rpy2.robjects import default_converter
#rvegan = importr('vegan')
#rbiocparallel = importr('BiocParallel')
#rdeseq2 = importr('DESeq2')
pandas2ri.activate()

import warnings
warnings.filterwarnings("ignore", category=RuntimeWarning)


N_JOBS = 6
resdir = "differential_analysis_results/"
tempdir = resdir+"tempdir/"

if not os.path.exists(resdir):
    os.mkdir(resdir)
if not os.path.exists(tempdir):
    os.mkdir(tempdir)

mapp = {    'Never':        0,
            'Rarely':       1,
            'Occasionally': 2,
            'Regularly':    3,
            'Daily':        4,
#            'true':         True,
#            'false':        False,
#            'True':         True,
#            'False':        False,
#            'TRUE':         True,
#            'FALSE':        False,
#            'YES':          True,
#            'Yes':          True,
#            'yes':          True,
#            'NO':           False,
#            'No':           False,
#            'no':           False,
            'I do not have this condition': False,
            'Self-diagnosed': True,
            'Diagnosed by a medical professional (doctor, physician assistant)': True,
            'Diagnosed by an alternative medicine practitioner': True,
            }

# precalculate
def diffab_cache(colname, abn=collM_c(), sam_md=get_sam_md(), tax_md=get_tax_md()):
#def diffab_cache(colname, abn=collM_c(), sam_md=get_sam_md(), tax_md=get_tax_md()):
    res = deseq_R_all_contrasts(colname, abn, sam_md, tax_md)
    return res


def limma_R(formula, md, group_names): # design_matrix, data):
    pass
    # limma wants samples as columns and measurements as rows
    #_ = design_matrix_R(formula, md, group_names)
    #return r('dm')
    ##r.assign('dm', design_matrix.T)
    #r.assign('df', data)

    #call = '''
    #        fit = lmFit(df, dm)\n

    #        conm = makeContrasts(young_ctrl-old_ctrl, young_ctrl-old_ss31, levels = dm)\n
    #        fit_con = contrasts.fit(fit, conm)\n
    #        fit_con <- eBayes(fit_con)\n

    #        topTable(fit_con, coef=1, adjust="BH")\n
    #        results <- decideTests(fit_con)\n
    #        '''
    #r(call)
    #return r('results')

def test_subset(sam_md, contrast_colname, n=20):
    ss = sam_md.copy()
    ss = sam_md.groupby(contrast_colname).apply(lambda x: x.sample(min(n, x.shape[0])))
    ss.index = ss.index.droplevel('biome')
    #ss = ss.groupby(contrast_colname).apply(lambda x: x if np.random.randint(2) else None)
    contrast_list = ['SOIL', 'FECAL', 'GUT', 'DAIRY PRODUCTS']
    ss = ss[[i in contrast_list for i in ss[contrast_colname]]]
    #x = x.head(3*n)
    return ss

def deseq_R(count_d, sam_md, tax_md,
            collapse_by = None,
            use_tax_level='taxonomy',
            sample_grouping_dict = None,
            contrast1 = None, contrast2 = None,
            contrast_colname = None,
            helper_fp = "helper", n_jobs = 6):
    '''
    Examples:

    contrast1        = "ERP016008_0", contrast2='ERP016008_1'
    contrast_colname = "group_word_count" OR "biome"
    count_d_fp       = '/D/ebi/A_OTU-TSV_v2_intestine.tsv'
    tax_md_fp        = '/D/ebi/taxonomy_metadata.tsv'
    sam_md_fp        = '/D/ebi/DEFAULT_METADATA.tsv'
    '''
    print("Preparing data for loading into R...")
    # not tested
    if isinstance(collapse_by, str):
        count_d = collM(count_d, collapse_cols=collapse_by)

    if isinstance(sample_grouping_dict, dict):
        # add a dummy column
        contrast_colname = 'contrast'
        contrast1 = tuple(sample_grouping_dict.keys())[0]
        contrast2 = tuple(sample_grouping_dict.keys())[1]

        contrast_column = pd.Series(
                index = list(sample_grouping_dict[contrast1])+list(sample_grouping_dict[contrast2]),
                data  = [contrast1]*len(sample_grouping_dict[contrast1])+\
                        [contrast2]*len(sample_grouping_dict[contrast2])
                )
        sam_md.insert(0, contrast_colname, contrast_column)
        sam_md = sam_md.applymap(str)
        sam_md = sam_md[sam_md[contrast_colname].notnull()]

    elif all([pd.notnull(i) for i in [contrast1, contrast2, contrast_colname]]):
        if isinstance(contrast1, bool):
            contrast1 = str(contrast1)
        if isinstance(contrast2, bool):
            contrast2 = str(contrast2)
        if isinstance(contrast_colname, bool):
            contrast_colname = str(contrast_colname)
        # continue as old
        pass
    else:
        raise ValueError("Specify either contrast colname and values or a dictionary\
                with contrast mapping of samples")

    sam_md = sam_md.ix[[not i for i in pd.isnull(sam_md[contrast_colname])],:]
    #sam_md = sam_md.drop('run_id', 1).drop_duplicates()
    sam_md = sam_md.ix[
                [i in [contrast1, contrast2] for i in sam_md[contrast_colname]],
                :]
    sam_md.index = sam_md['sample_id']
    sam_md = sam_md.drop_duplicates()
    tax_md.index = tax_md[use_tax_level]
    tax_md = tax_md[[use_tax_level]].drop_duplicates()

    count_d, sam_md = count_d.align(sam_md, axis=0, join='inner')
    count_d = count_d.T
    count_d, tax_md = count_d.align(tax_md, axis=0, join='inner')
    assert sk.delete_empty(count_d).shape > (0,0), "No (overlapping) samples annotated with these contrasts"

    count_data_fp_R = tempdir+'count_data_R.tsv'
    sample_metadata_fp_R = tempdir+'sample_metadata_R.tsv'
    taxonomy_metadata_fp_R = tempdir+'taxonomy_metadata_R.tsv'

#    count_data_fp = paste0(tempdir,'count_data_R.tsv')
#    sample_metadata_fp = paste0(tempdir,'sample_metadata_R.tsv')
#    taxonomy_metadata_fp = paste0(tempdir,'taxonomy_metadata_R.tsv')

    count_d.to_csv(count_data_fp_R, sep="\t")
    sam_md.to_csv(sample_metadata_fp_R, sep="\t")
    tax_md.to_csv(taxonomy_metadata_fp_R, sep="\t")

    print("Prepraring the R environment...")
    r.assign('sample_metadata_fp', sample_metadata_fp_R)
    r.assign('count_data_fp', count_data_fp_R)
    r.assign('taxonomy_metadata_fp', taxonomy_metadata_fp_R)
    r.assign('contrast_colname', contrast_colname)
    r.assign('contrast1', contrast1)
    r.assign('contrast2', contrast2)
    r.assign('n_jobs', n_jobs)

    print("Calculating differential abundance...")
    r("source('"+helper_fp+"/differential.R')")
    r("run_deseq(count_data_fp, sample_metadata_fp, taxonomy_metadata_fp,\
                contrast_colname, contrast1, contrast2, n_jobs)")
    results = pd.DataFrame(r('deseq_results'))
    sample_metadata = pd.DataFrame(r('deseq_sample_metadata'))
    geomeans = pd.DataFrame(r('deseq_geomeans'))

    res = {'results':results, 'sample_metadata': sample_metadata,
                'geomeans': geomeans}
    #return results, sample_metadata, geomeans
    print("Success.")
    return res


def deseq_R_all_contrasts(contrast_colname,
                count_d, sam_md, tax_md,
                conditions = 'all',
                ):

    #count_d = pd.read_csv(count_d_fp, sep='\t', index_col=0)
#    sam_md  = pd.read_csv(sam_md_fp, sep="\t",
#                index_col=False, low_memory=False,
#                usecols=list(set(['project_id', 'biome', 'sample_id', 'sample_name',
#                        'keywords', contrast_colname, 'group_word_count']))
#            ).drop_duplicates()
#    tax_md  = pd.read_csv(tax_md_fp, sep='\t', index_col=0)

    sam_md = sam_md[list(set(['project_id', 'biome', 'sample_id', 'sample_name',
                        contrast_colname]))]\
                        .drop_duplicates()
    sam_md.index = sam_md['sample_id']
    sam_md = sam_md.ix[count_d.index]

    count_d, sam_md = count_d.align(sam_md, axis=0, join='inner')


    # testing
    #sam_md = sam_md[[i in ['Fecal', 'Soil'] for i in sam_md.biome]]
    #sam_md = sam_md.sample(min(100, sam_md.shape[0]))
    # /testing

    if conditions == 'all':
        conditions = set(sam_md[contrast_colname])
        print('There are', len(conditions), 'conditions')
    else:
        assert all(conditions in sam_md[contrast_name])
        print('#TODO: subset!')
        sys.exit()


    deseq_results = {}
    erron = []
    n_combs = 0

    for c1, c2 in combinations(conditions, r=2):
        print("-"*80)
        print(c1, c2)
        n_combs += 1

        try:
            deseq_res = deseq_R(count_d, sam_md, tax_md,
                c1, c2,
                contrast_colname=contrast_colname)
            deseq_res = deseq_res['results']
            #TODO get no of common taxa: sm = deseq_res['sam_md']; A.groupby(sm.biome).sum()
            #print(deseq_res.head())
            deseq_results["-".join([c1, c2]).replace(" ", "_").lower()] = deseq_res

        except AssertionError as e:
            print(e)
            print("No overlapping taxa for these conditions.")
        except:
            print('Some problem for', c1, c2)
            #print(e)
            erron.append((c1,c2))

    print("-"*80)
    print('Number of conditions:', len(conditions))
    print('Number of combinations:', n_combs)
    print('Errors on', erron)

    #diffab_matrix_from_deseq(deseq_results).to_csv(resdir+"DA.tsv", sep="\t")
    print("-"*80)
    print('Saving results to files')
    for pair in deseq_results.keys():
        fname = resdir+"differential_abundance_"+str(pair)+".tsv"
        print(fname)
        deseq_results[pair].to_csv(fname, sep='\t')

    return deseq_results


def list_of_samples2contrast_colname(dict_of_lists_of_samples):
    pass


def amgut2y(amgut_md_fp = AMGUT_MD_FP, contrasts=False, incl_continuous=True):
    def resolve_bool(x, reverse=False, other=np.nan):
        if x == True or x == False:
            if reverse:
                return not x
            else:
                return x
        else:
            return other

    def resolve_str(x, other=np.nan):
        if x == "Never" or x == 'I do not have this condition':
            return False
        elif isinstance(x,str):
            if x.split(' ')[0] in mapp.keys():
                return True
        else:
            return other

    def resolve_bool_contrasts(x, other=np.nan):
        if x == True:
            return 'Tr'
        elif x == False:
            return 'Fa'
        else:
            return other


    amgut_md = get_amgut_md(amgut_md_fp)

    new_md = []


    for var in amgut_md.columns:
        levels = amgut_md[var].value_counts().index

        new = None

        if True in levels:
            # with SUBSET variables, True means no condition!
            # amgut_md[['SUBSET_IBD', 'IBD']].sample(10)
            if var.startswith('SUBSET'):
                new = amgut_md[var].apply(lambda x: resolve_bool(x, reverse=True))
            else:
                new = amgut_md[var].apply(lambda x: resolve_bool(x, reverse=False))
            if contrasts:
                new = new.apply(resolve_bool_contrasts)

        elif any([k in levels for k in mapp.keys()]):
            if 'Never' in levels:
                if contrasts:
                    new = amgut_md[var].apply(resolve_str)
                else:
                    new = amgut_md[var].apply(lambda x: x.split(' ')[0] if isinstance(x,str) else x)
                    new = new.apply(lambda x: mapp[x] if x in mapp.keys() else np.nan)
            else:
                new = amgut_md[var].apply(lambda x: mapp[x] if x in mapp.keys() else np.nan)
                if contrasts:
                    new = new.apply(resolve_bool_contrasts)
        else:
            if not contrasts:
                if incl_continuous:
                    try:
                        new = pd.to_numeric(amgut_md[var])
                    except ValueError:
                        pass

        if isinstance(new, pd.Series):
            new_md.append(new)

    new_md = pd.concat(new_md, axis=1)
    new_md.drop_duplicates(inplace=True)
    return new_md


def contrasts_data_deseq2(amgut_md_fp=AMGUT_MD_FP):
    return amgut2y(contrasts=True)

def check_md(which = "continuous", amgut_md_fp=AMGUT_MD_FP):
    ag0 = get_amgut_md(amgut_md_fp)
    if which == 'continuous':
        ag1 = amgut2y()
    elif which == 'contrasts':
        ag1 = amgut2y(contrasts = True)

    ag0,ag1 = ag0.align(ag1, axis=0, join='inner')


    wrong = []
    for c in ag1:
        x = (ag0[c].apply(pd.isna) != ag1[c].apply(pd.isna)).sum()
        if x != 0:
            wrong.append(pd.concat(ag0[c].align(ag1[c], join='inner'),1))

    return pd.concat(wrong, 1)





def amgut_compute_all_contrasts(data='taxonomy', A=None,
    collapse_by = None,
    contrasts_md=None, tax_md=None,
    diffab_resdir = '/P/mnemonic/differential_analysis_results',
    ofile_suffix = '',
    n_jobs=8):

    if not os.path.isdir(diffab_resdir):
        os.mkdir(diffab_resdir)

    if not isinstance(tax_md, pd.DataFrame):
        if data == 'taxonomy':
            tax_md = get_tax_md()
        if data == 'function':
            tax_md = get_go_md()
            #FIXME
            tax_md.columns = ['taxonomy']
    if not isinstance(contrasts_md, pd.DataFrame):
        contrasts_md = contrasts_data_deseq2()
    if not isinstance(A, pd.DataFrame):
        print('Loading data...')
        if data == 'taxonomy':
            A = pd.read_csv('/D/ebi/A_OTU-TSV_v2.tsv', sep='\t', index_col=0,)
        elif data == 'function':
            A = pd.read_csv('/D/ebi/G_GOAnnotations_v2.tsv', sep='\t', index_col=0)

    if isinstance(collapse_by, str): # won't work for functional data
        A = sk.collM(A, collapse_cols=collapse_by)

    print(A.head())

    A.drop_duplicates(inplace=True)
    #A = preprocess.quantile_transform_pd(A)#, axis=1) # transform samples rather than features?
    print(contrasts_md.shape)

    contrasts_md, A = contrasts_md.align(A, join='inner', axis=0)

    print('N contrasts', contrasts_md.shape[1])

    #ATT subsettign
    for var in contrasts_md.columns:
        if var == 'sample_id':
            continue
        if contrasts_md[var].dropna().value_counts().shape[0] == 1:
            print(var, "only has one level")
            continue

        if data == 'taxonomy':
            filename = os.path.join(diffab_resdir,
                "differential_abundance_{0}_taxonomy{1}.tsv".format(var, ofile_suffix))
        if data == 'function':
            filename = os.path.join(diffab_resdir,
                "differential_abundance_{0}_function{1}.tsv".format(var, ofile_suffix))
        print(var)
        print("Saving to", filename)


        contrasts_md_ = contrasts_md.groupby(var)\
            .apply(lambda x: x.sample(min(500, x.shape[0]), axis=0))\
            .reset_index(level=var, drop=True)

        contrasts_md_, A_ = contrasts_md_.align(A, join='inner', axis=0)
        contrasts_md_.insert(0, 'sample_id', contrasts_md_.index)
        print('A shape', A_.shape)
        print('Contrasts shape', contrasts_md_.shape)
        #print('\n')

        try:
            dr = deseq_R(A_, contrasts_md_, tax_md,
                        contrast1 = 'Tr', contrast2="Fa", contrast_colname=var,
                        use_tax_level = 'genus',
                        n_jobs=n_jobs
                        )
            dr['results'].to_csv(filename, sep='\t')

        except AssertionError as e:
            print(e)

        print('\n')




if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="\
                        Compute all contrasts for a given abundance matrix with DESeq2.\
                        ")
#    parser.add_argument("-a", "--abundance_matrix_fp",
#                        default="",
#                        help="",
#                        required=False)
    parser.add_argument("-d", "--data",
                        default="taxonomy",
                        help="",
                        required=False)
    parser.add_argument("-b", "--collapse_by",
                        default = None,
                        help="",
                        required=False)
#    parser.add_argument("-c", "--contrasts_md",
#                        default="",
#                        help="",
#                        required=False)
#    parser.add_argument("-t", "--taxonomy_md",
#                        default="",
#                        help="",
#                        required=False)
    parser.add_argument("-o", "--output_dir",
                        default = '/P/mnemonic/differential_analysis_results',
                        help="",
                        required=False)
    parser.add_argument("-s", "--ofile_suffix",
                        default = '',
                        help="",
                        required=False)
    args = vars(parser.parse_args())

    data = args['data']
    odir = args['output_dir']
    ofile_suffix = args['ofile_suffix']

    if not os.path.isdir(odir):
        os.mkdir(odir)

    amgut_compute_all_contrasts(data=data,
        collapse_by = collapse_by,
        diffab_resdir = odir,
        ofile_suffix = ofile_suffix,
        n_jobs=8)



