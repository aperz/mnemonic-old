#!/usr/bin/env python3

'''
docs?
'''

#TODO sanity check on the Projects_...txt file if the columns are consistne (there are "unnamed" columns?)
# check on the ebi website if really I have ALL the projects

import subprocess
import pandas as pd
import argparse
import os

from ebi_summarize_data import get_list_of_projects


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Wrapper for MGPortal bulk download tool to get ALL the projects.")
    parser.add_argument("-o", "--output_path",
                        help="Location of the output directory, where the downloadable files get stored.**MANDATORY**",
                        required=True)
    parser.add_argument("-v", "--version", help="Version of the pipeline used to generate the results.**MANDATORY**",
                        required=True)
    parser.add_argument("-t", "--file_type",
                        help="See mgportal_bulk_download.py --help for help.",
                        required=False)
    args = vars(parser.parse_args())

    #project_list_file = "/dat2/ebi/Projects_samples_runs.txt"
    project_information_url = "https://www.ebi.ac.uk/metagenomics/projects/doExportDetails?search=Search&studyVisibility=ALL_PUBLISHED_PROJECTS"

    print("="*80)
    #print("WARNING! Overriding list of all projects with list of intestine-related projects!")
    #print("WARNING! Downloading only All - Intestine projects (non-intestine)")
    print("WARNING! Downloading only new projects (no redownloading)")
    print("="*80)
    list_of_projects_all = get_list_of_projects(project_information_url)
    from metadata_keywords import has_keyword
    md = pd.read_csv('/D/ebi/DEFAULT_METADATA.tsv', sep='\t', index_col=False, low_memory=False)
    #list_of_projects_intestine = set(md.project_id[has_keyword(md, 'intestine').tolist()])
    proj_to_remove = set(md.project_id)
    #list_of_projects_intestine = set(md.project_id[has_keyword(md, 'intestine').tolist()])

    #proj_to_remove = [
    #    "ERP000108",
    #    "ERP000118",
    #    "ERP000339",
    #    "ERP000554",
    #    "ERP000673",
    #    "ERP000674",
    #    "ERP001021",
    #    "ERP001038",
    #    "ERP001068",
    #    "ERP001178",
    #    "ERP001224",
    #    "ERP001227",
    #    "ERP001384",
    #    "ERP001506",
    #    "ERP001568",
    #    "ERP001596",
    #    "ERP001706",
    #    "ERP001736"
    #]
    #proj_to_remove = []

    list_of_projects = [p for p in list_of_projects_all if p not in proj_to_remove]
    #print(len(list_of_projects_all))
    #print(len(proj_to_remove))
    #print(len(list_of_projects))

    print("---Downloading data for ", len(list_of_projects), " projects")

    for project_id in list_of_projects:
        print(project_id, args['output_path'], args['version'], args['file_type'])
        subprocess.call( \
            "python2 mgportal_bulk_download.py -p {0} -o {1} -v {2} -t {3}"\
                .format(project_id, args['output_path'], args['version'], args['file_type']),
            shell=True
            )

    print("---The next step is to pull out all the .csv files from directories with\
            regular_files_extract.py.")

