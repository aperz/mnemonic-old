#!/usr/bin/env python3

import pandas as pd
import numpy as np
from scipy.spatial import distance_matrix
from sklearn.metrics import mutual_info_score
from sklearn.preprocessing import normalize
import argparse

### PART I: transform
# collapsing (overlapping functionality to abu_mat_from.py),
# dimensionality reduction.py, ordination.R (pca, ...), normalization

def infer_columns_name(A, md):
    x = apply(lambda x: sum([i in x.tolist() for i in A.index])
    A.columns.name = x[x == max(x)].index.tolist()[0]
    return(A)


#def get_mapping(md, col1, col2):
#    #output dict? Mulilevel index? Probably not much sense
#    return md[[col1, col2]]


def add_multiindex(A, md, row_or_col, new_level):
    '''
    row_or_col: 'row', 'col'
    '''
    mix = md[new_level]
    if row_or_col == "col":
        mix.index = md[A.columns.name]
    else: # row_or_col == "row" or not specified
        mix.index = md[A.index.name]

    mix_, A_ = mix.align(A, join="inner")

    mix_ = pd.MultiIndex.from_arrays([mix_.index, mix_])
    A_.index = mix_
    return A_


def collapse_by(A, by, md, row_or_col="row"):
    #TODO
    #TODO in md include all levels of phylo tree (so collapsing by e.g. phylum etc)
    # get mapping form md
    A = add_multiindex(A, md, row_or_col, by)
    return A


def normalize_pandas(A, axis=0):
    #TODO: check: leave zeros be for th tax abundace?
    return pd.DataFrame(
        normalize(A, axis=axis), # sklearn
        columns = A.columns,
        index  = A.index,
    )


### PART II distances etc: change the essence of information; rows or cols
# correspondence_analysis.py, correlation, MI distance_make.py

#TODO
# mahalanobis
# log

def correlation_pandas(A, axis=0):
    # not tested
    if axis == 1:
        A = M.T

    return pd.DataFrame(
        np.corrcoef(A), # mulithreaded? O.o
        columns = A.index,
        index  = A.index,
        )

# should transform poisson por neg bin
def mutual_information_2D(x, y, bins):
    '''
    for discrete data
    '''
    c_xy = np.histogram2d(x, y, bins)[0]
    mi = mutual_info_score(None, None, contingency=c_xy)

    return mi


def mutual_information_pandas(A, axis=0, bins=8):
    # for math see https://gist.github.com/GaelVaroquaux/ead9898bd3c973c40429
    # doesn't need / want normalization?
    #TODO parallelize! and calculate half matrix?

    if axis == 1:
        A = M.T

    n = A.shape[axis]
    MI = np.zeros((n,n))

    for i in range(n-1): # why -1? dunno.
        for j in range(i+1, n-1):
            MI[i,j] = mutual_information_2D(A.ix[:,i], A.ix[:,j], bins)

    return pd.DataFrame(MI,
        columns = A.index,
        index  = A.index,
        )


# former correspondence_analysis.py
def regularise(A):
    N = A.sum(0).sum()
    A_expected = pd.DataFrame(
        np.outer(A.apply(sum, 1), A.apply(sum, 0)),
        columns = A.columns, index = A.index
        )\
        .applymap(lambda x: x / N )\

    A_regularised = A * A_expected / A_expected**(1/2)

    return(A_regularised)

def correspondence_analysis(A, axis):
    '''
    Assumes Poisson distribution of variables, regularises them.
    Returns an nrow x nrow matrix of distances between rows.
    The input matrixmust have no zero col sums nor row sums! (else A_expected has zeros)

    Input:
        A:
                Matrix of counts / frequencies / etc.
        axis:
                Whether distances between rows ("row") or columns ("col"),
                just the regularised matrix ("reg"),
                or just the subsetted matrix ("subset") are ruturned.
    Output:
                Matrix of distances.
    '''

    #A = pd.read_csv(ifile_path, sep="\t")
    A =A
    A.rename(columns={'Unnamed: 0':'unknown'}, inplace=True)
    #A.drop('Unnamed: 0', axis = 1, inplace=True)
    A = A.ix[:, list(A.sum(0) > 0)]
    A = A.ix[list(A.sum(1) > 0), :]
    A = A.ix[:, list(A.sum(0) > 0)]
    A = A.ix[list(A.sum(1) > 0), :]

    if axis == "subset":
        A_out = A

    A_reg = regularise(A)

    if axis == "reg":
        A_out = A_reg

    elif axis == "col":
    # euclidean distance
        A_out = pd.DataFrame(distance_matrix(A_reg.T, A_reg.T), #A_reg for samples
            columns = A_reg.columns, index = A_reg.columns)

    elif axis == "row":
        A_out = pd.DataFrame(distance_matrix(A_reg, A_reg), #A_reg for samples
            columns = A_reg.index, index = A_reg.index)

    #else:
    #    print("Specify axis.")
    return A_out
    #A_out.to_csv(ofile_path, sep="\t")



# PART III
# the shifts matrix is essentially the same (rows: sample-sample, cols: taxonomy).
# Here: some specific functions for those? (It's mostly implemented in biom_stuff.py


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="\
                        TODO: implement orditnation  \
                        Do various things to the abundance matrix of the format roughly sample x taxonomy\
                        #Functions to collapse the counts by other metadata columns.\
                        ")
    parser.add_argument("-n", "--normalise_or_not",
                        help="",
                        required=False)
    parser.add_argument("-i", "--input_path",
                        help="",
                        required=True)
    parser.add_argument("-o", "--output_path",
                        help="",
                        required=True)
    #parser.add_argument("-c1", "--column_name1"
    #                    required = True,
    #                    help="The name of the (count) column whose values should appear as rows of the generated count matrix.")
    #parser.add_argument("-c2", "--column_name2",
    #                    required=True,
    #                    help="The name of the column whose values should appear as columns.")
    #parser.add_argument("-m", "--metadata",
    #                    required=True,
    #                    help="The name of the column whose values should appear as columns.")
    args = vars(parser.parse_args())

    ifile_path = args['input_path']
    ofile_path = args['output_path']
    #normalise_or_not = args['normalise_or_not']


    A = pd.read_csv(ifile_path, sep="\t", index_col = 0)
    A = infer_columns_name(A, md)



